exports.emergencyCall = function() {
    var etel = Titanium.UI.createAlertDialog({
        title: "Emergency Call Confirmation",
        message: "Are you sure you want to call the Emergency Services?",
        cancel: 0,
        buttonNames: [ "No", "Yes" ]
    });
    etel.addEventListener("click", function(e) {
        if (1 == e.index && true) if (true == Ti.Platform.canOpenURL("tel:01246")) Titanium.Platform.openURL("tel:01246"); else {
            var msg = Titanium.UI.createAlertDialog({
                title: "Warning",
                message: "This device cannot make phone calls"
            });
            msg.show();
        } else if (1 == e.index && false) {
            var intent = Ti.Android.createIntent({
                action: Ti.Android.ACTION_CALL,
                data: "tel:01246"
            });
            Ti.Android.currentActivity.startActivity(intent);
        }
    });
    etel.show();
};