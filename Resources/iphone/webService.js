function callWebService(url, params) {
    var args = params || null;
    var xhr = Titanium.Network.createHTTPClient();
    xhr.setTimeout(1e4);
    xhr.open("GET", url);
    xhr.setRequestHeader("Accept", "application/json; charset=utf-8");
    xhr.onerror = function(e) {
        Ti.API.fireEvent("webServiceResponse", {
            status: "error",
            response: e
        });
    };
    xhr.onload = function() {
        var obj = this.responseText;
        Ti.API.fireEvent("webServiceResponse", {
            status: "success",
            response: obj
        });
    };
    null == args ? xhr.send() : xhr.send(JSON.stringify(args));
}

module.exports.callWebService = callWebService;