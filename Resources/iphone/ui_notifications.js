function createLoadingIndicator(args) {
    function openIndicator() {
        globals.isiOs() && win.open();
        activityIndicator.show();
    }
    function closeIndicator() {
        activityIndicator.hide();
        globals.isiOs() && win.close();
    }
    var width = 50, height = 50;
    var args = args || {};
    args.text || "Loading ...";
    if (globals.isAndroid()) {
        var activityIndicator = Ti.UI.createActivityIndicator({
            style: Ti.UI.ActivityIndicatorStyle.DARK,
            height: Ti.UI.FILL,
            width: 30,
            color: "green",
            font: {
                fontFamily: "Helvetica Neue",
                fontSize: "26sp",
                fontWeight: "bold"
            }
        });
        activityIndicator.openIndicator = openIndicator;
        activityIndicator.closeIndicator = closeIndicator;
        return activityIndicator;
    }
    var activityIndicator = Ti.UI.createActivityIndicator({
        style: Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN,
        left: 0,
        height: Ti.UI.FILL,
        width: 30
    });
    var win = Titanium.UI.createWindow({
        height: height,
        width: width,
        borderRadius: 10,
        touchEnabled: false,
        backgroundColor: "#000",
        opacity: .6
    });
    var view = Ti.UI.createView({
        width: Ti.UI.SIZE,
        height: Ti.UI.FILL,
        center: {
            x: width / 2,
            y: height / 2
        },
        layout: "horizontal"
    });
    Titanium.UI.createLabel({
        left: 10,
        width: Ti.UI.FILL,
        height: Ti.UI.FILL,
        color: "#fff",
        font: {
            fontFamily: "Helvetica Neue",
            fontSize: "16sp",
            fontWeight: "bold"
        }
    });
    view.add(activityIndicator);
    win.add(view);
    win.openIndicator = openIndicator;
    win.closeIndicator = closeIndicator;
    return win;
}

function displayToast(toast) {
    if (globals.isAndroid()) {
        var toast = Ti.UI.createNotification({
            message: toast,
            duration: Ti.UI.NOTIFICATION_DURATION_LONG
        });
        toast.show();
    } else if (globals.isiOs()) {
        var indWin = Titanium.UI.createWindow();
        var indView = Titanium.UI.createView({
            height: Ti.UI.SIZE,
            width: Ti.UI.SIZE,
            borderRadius: 3,
            backgroundColor: "#000",
            opacity: .7
        });
        indWin.add(indView);
        var message = Titanium.UI.createLabel({
            text: toast,
            color: "#fff",
            width: Ti.UI.SIZE,
            height: Ti.UI.SIZE,
            textAlign: "center",
            font: {
                fontFamily: "Helvetica Neue",
                fontSize: "12sp",
                fontWeight: "bold"
            }
        });
        indView.add(message);
        indWin.open();
        setTimeout(function() {
            indWin.remove(indView);
            indWin.close();
        }, IPAD_TOAST_DURATION);
    }
}

var globals = require("globals");

var indicator = null;

var IPAD_TOAST_DURATION = 3e3;

exports.hideLoadingIndicator = function() {
    null != indicator && globals.isiOs() && indicator.closeIndicator();
};

exports.showLoadingIndicator = function(args) {
    indicator = createLoadingIndicator(args);
    if (globals.isiOs()) indicator.openIndicator(); else {
        var text = args.text || "Loading ...";
        displayToast(text);
    }
};

exports.displayToast = function(toast) {
    displayToast(toast);
};