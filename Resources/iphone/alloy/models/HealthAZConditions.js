var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

exports.definition = {
    config: {
        columns: {
            id: "INTEGER PRIMARY KEY",
            conditionText: "TEXT",
            conditionURI: "TEXT",
            conditionParent: "INTEGER"
        },
        adapter: {
            type: "sql",
            collection_name: "healthAZConditions",
            idAttribute: "id"
        }
    }
};

model = Alloy.M("healthAZConditions", exports.definition, []);

collection = Alloy.C("healthAZConditions", exports.definition, model);

exports.Model = model;

exports.Collection = collection;