function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "messageView";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.messageView = Ti.UI.createView({
        zIndex: 20,
        backgroundColor: Alloy.CFG.colors.messageViewColor,
        borderColor: "#ffffff",
        borderWidth: 2,
        width: 500,
        height: 300,
        id: "messageView"
    });
    $.__views.messageView && $.addTopLevelView($.__views.messageView);
    $.__views.messageText = Ti.UI.createLabel({
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        width: 450,
        color: "#ffffff",
        font: Alloy.CFG.fonts.igscSmall,
        text: "The Battlecard for your selection is not available yet.\n\nPlease use the button at the top right of the screen to go to the IGSC Wiki where realevant information on this subject can be found.",
        id: "messageText"
    });
    $.__views.messageView.add($.__views.messageText);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;