function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "line";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.containerId = Ti.UI.createView({
        top: 0,
        id: "containerId"
    });
    $.__views.containerId && $.addTopLevelView($.__views.containerId);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var horizontalLine = true;
    if (args.horizontal) {
        horizontalLine = true;
        $.containerId.top = 0;
        $.containerId.left = 0;
        $.containerId.width = "100%";
        $.containerId.height = 1;
    } else {
        horizontalLine = false;
        $.containerId.top = 0;
        $.containerId.left = 0;
        $.containerId.width = 1;
        $.containerId.height = "100%";
    }
    $.containerId.backgroundColor = args.backgroundColor ? args.backgroundColor : "#ff0000";
    null != args.left && ($.containerId.left = args.left);
    null != args.right && ($.containerId.right = args.right);
    null != args.width && ($.containerId.width = args.width);
    (null != args.bottom || 0 == args.bottom) && ($.containerId.bottom = args.bottom);
    null != args.top && ($.containerId.top = args.top);
    null != args.zIndex && ($.containerId.zIndex = args.zIndex);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;