function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "menuTile";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.menuTile = Ti.UI.createView({
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        id: "menuTile"
    });
    $.__views.menuTile && $.addTopLevelView($.__views.menuTile);
    $.__views.background = Ti.UI.createView({
        id: "background"
    });
    $.__views.menuTile.add($.__views.background);
    $.__views.icon = Ti.UI.createView(function() {
        var o = {};
        Alloy.isTablet && _.extend(o, {
            backgroundImage: "icons/white/ambulance.png",
            height: 120,
            width: 120,
            bottom: 5,
            right: 5
        });
        Alloy.isHandheld && _.extend(o, {
            backgroundImage: "icons/white/ambulance.png",
            height: 40,
            width: 40,
            bottom: 5,
            left: 2
        });
        _.extend(o, {
            id: "icon"
        });
        return o;
    }());
    $.__views.menuTile.add($.__views.icon);
    $.__views.colorLayer = Ti.UI.createView({
        backgroundColor: "#ee00aaff",
        id: "colorLayer"
    });
    $.__views.menuTile.add($.__views.colorLayer);
    $.__views.tileText = Ti.UI.createLabel(function() {
        var o = {};
        Alloy.isTablet && _.extend(o, {
            touchEnabled: false,
            top: "10%",
            left: 5,
            right: 5,
            color: "#ffffff",
            font: {
                fontName: "helvetica neau light",
                fontSize: 30,
                fontWeight: "light"
            },
            text: "Not Set Yet"
        });
        Alloy.isHandheld && _.extend(o, {
            touchEnabled: false,
            top: "10%",
            left: 5,
            right: 5,
            color: "#ffffff",
            font: {
                fontName: "helvetica neau light",
                fontSize: 20,
                fontWeight: "light"
            },
            text: "Not Set Yet"
        });
        _.extend(o, {
            id: "tileText",
            text: "No Text Set"
        });
        return o;
    }());
    $.__views.menuTile.add($.__views.tileText);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    arguments[1] || false;
    var solidBackgroundColor = "#00000000";
    var vOpacity = "88";
    if (!("" === args.backgroundColor && 9 === args.backgroundColor.length)) {
        solidBackgroundColor = "#" + vOpacity + args.backgroundColor.substring(3);
        $.background.backgroundColor = solidBackgroundColor;
    }
    "" === args.icon || ($.icon.backgroundImage = args.icon);
    "" === args.backgroundColor || ($.colorLayer.backgroundColor = args.backgroundColor);
    args.font && ($.tileText.font = args.font);
    "" === args.title || ($.tileText.text = args.title);
    0 === args.padTop || "" === args.padTop || ($.menuTile.top = args.padTop);
    0 === args.padLeft || "" === args.padLeft || ($.menuTile.left = args.padLeft);
    0 === args.padBottom || "" === args.padBottom || ($.menuTile.bottom = args.padBottom);
    0 === args.padRight || "" === args.padRight || ($.menuTile.right = args.padRight);
    if (!("" === args.menuMetaData)) {
        $.colorLayer.tagName = args.menuMetaData.tagName;
        $.colorLayer.fileType = args.menuMetaData.fileType;
        $.colorLayer.version = args.menuMetaData.version;
        $.colorLayer.fileName = args.menuMetaData.fileName;
    }
    "string" != typeof args.docPath && (args.docPath = "");
    $.colorLayer.docPath = "" + args.menuMetaData.fileName;
    "" === args.webPath || ($.colorLayer.webPath = args.webPath);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;