function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function changeLink() {
        messageView && $.movieViewer.remove(messageView);
        showIndicator();
    }
    function hideIndicator() {
        ui_notifications.hideLoadingIndicator($.movieViewer);
    }
    function showIndicator() {
        $.linkBtn.enabled = false;
        var args = {
            text: "Loading..."
        };
        ui_notifications.showLoadingIndicator(args, $.movieViewer);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "movieViewer";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.movieViewer = Ti.UI.createWindow({
        backgroundColor: "white",
        id: "movieViewer"
    });
    $.__views.movieViewer && $.addTopLevelView($.__views.movieViewer);
    $.__views.linkBtn = Ti.UI.createButton({
        id: "linkBtn",
        title: "IGSC - Wiki"
    });
    changeLink ? $.__views.linkBtn.addEventListener("click", changeLink) : __defers["$.__views.linkBtn!click!changeLink"] = true;
    $.__views.movieViewer.rightNavButton = $.__views.linkBtn;
    $.__views.videoPlayer = Ti.Media.createVideoPlayer({
        id: "videoPlayer",
        top: "50",
        url: "pdf/Emirates.mp4",
        height: "300",
        width: "700",
        backgroundColor: "white",
        autoplay: "true"
    });
    $.__views.movieViewer.add($.__views.videoPlayer);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var ui_notifications = require("ui_notifications");
    var docPath = "notthisquarter.png";
    var webPath = "https://www.c3.csc.com";
    if ("" != args.docPath) docPath = args.docPath; else {
        var messageView = Alloy.createController("messageView").getView();
        $.movieViewer.add(messageView);
    }
    args.webPath && (webPath = args.webPath);
    $.videoPlayer.addEventListener("load", function() {
        hideIndicator();
    });
    __defers["$.__views.linkBtn!click!changeLink"] && $.__views.linkBtn.addEventListener("click", changeLink);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;