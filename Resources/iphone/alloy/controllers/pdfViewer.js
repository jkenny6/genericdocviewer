function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function changeLink() {
        messageView && $.pdfViewer.remove(messageView);
        showIndicator();
        Alloy.Globals.igscWebView.url = webPath;
    }
    function hideIndicator() {
        ui_notifications.hideLoadingIndicator($.pdfViewer);
    }
    function showIndicator() {
        $.linkBtn.enabled = false;
        var args = {
            text: "Loading..."
        };
        ui_notifications.showLoadingIndicator(args, $.pdfViewer);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "pdfViewer";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.pdfViewer = Ti.UI.createWindow({
        backgroundColor: "white",
        id: "pdfViewer"
    });
    $.__views.pdfViewer && $.addTopLevelView($.__views.pdfViewer);
    $.__views.linkBtn = Ti.UI.createButton({
        id: "linkBtn",
        title: "IGSC - Wiki"
    });
    changeLink ? $.__views.linkBtn.addEventListener("click", changeLink) : __defers["$.__views.linkBtn!click!changeLink"] = true;
    $.__views.pdfViewer.rightNavButton = $.__views.linkBtn;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var ui_notifications = require("ui_notifications");
    var docPath = "notthisquarter.png";
    var webPath = "https://www.c3.csc.com";
    Alloy.Globals.igscWebView.width = Ti.UI.FILL;
    Alloy.Globals.igscWebView.height = Ti.UI.FILL;
    $.pdfViewer.add(Alloy.Globals.igscWebView);
    if ("" != args.docPath) docPath = args.docPath; else {
        var messageView = Alloy.createController("messageView").getView();
        $.pdfViewer.add(messageView);
    }
    args.webPath && (webPath = args.webPath);
    Alloy.Globals.igscWebView.url = docPath;
    Alloy.Globals.igscWebView.addEventListener("load", function() {
        hideIndicator();
    });
    __defers["$.__views.linkBtn!click!changeLink"] && $.__views.linkBtn.addEventListener("click", changeLink);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;