function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function buildMenu(menuData) {
        $.menuTitle.text = menuData.menu.menuTitle;
        $.titleControlContainer.backgroundColor = menuData.menu.titleColor;
        $.genericMenu.title = menuData.menu.menuTitle;
        var tilesMenuView = Alloy.createController("tiledMenu", menuData.menu).getView();
        $.tiledMenuContainer.add(tilesMenuView);
        var pathsDictionary = {};
        tilesMenuView.addEventListener("click", function(e) {
            pathsDictionary.docPath = "";
            pathsDictionary.webPath = "";
            var nextLevelDownMetaData = {
                tagName: e.source.tagName,
                category: e.source.fileType,
                fileType: e.source.fileType,
                version: e.source.version,
                fileName: e.source.fileName
            };
            if ("menudata" == nextLevelDownMetaData.fileType) openNextLevelDownMenu(nextLevelDownMetaData); else {
                e.source.docPath && (pathsDictionary.docPath = e.source.docPath);
                e.source.webPath && (pathsDictionary.webPath = e.source.webPath);
                "" == pathsDictionary.docPath ? displayPdfCallBack(nextLevelDownMetaData, pathsDictionary) : openPdf(nextLevelDownMetaData, pathsDictionary);
            }
        });
    }
    function openNextLevelDownMenu(menuMetaData) {
        var win = null;
        win = Alloy.createController("genericMenu", menuMetaData).getView();
        Alloy.Globals.navWin.openWindow(win, {
            animate: true
        });
    }
    function openPdf(nextLevelDownMetaData, pathsDictionary) {
        fileManager.getPdf(nextLevelDownMetaData, pathsDictionary, displayPdfCallBack);
    }
    function displayPdfCallBack(nextLevelDownMetaData, pathsDictionary) {
        alert(nextLevelDownMetaData);
        win = Alloy.createController("pdfViewer", pathsDictionary).getView();
        Alloy.Globals.navWin.openWindow(win, {
            animate: true
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "genericMenu";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.genericMenu = Ti.UI.createWindow({
        backgroundImage: "formula1Racing2BW.png",
        title: "",
        id: "genericMenu"
    });
    $.__views.genericMenu && $.addTopLevelView($.__views.genericMenu);
    $.__views.titleControlContainer = Ti.UI.createView({
        backgroundColor: "#333333",
        borderColor: "white",
        width: Ti.UI.SIZE,
        height: 30,
        id: "titleControlContainer"
    });
    $.__views.menuTitle = Ti.UI.createLabel({
        width: 300,
        color: "white",
        font: Alloy.CFG.fonts.igscSmall,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        id: "menuTitle"
    });
    $.__views.titleControlContainer.add($.__views.menuTitle);
    $.__views.genericMenu.titleControl = $.__views.titleControlContainer;
    $.__views.tiledMenuContainer = Ti.UI.createView({
        id: "tiledMenuContainer"
    });
    $.__views.genericMenu.add($.__views.tiledMenuContainer);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var fileManager = require("fileManager");
    var menuMetaData = arguments[0] || {};
    fileManager.getMenuData(menuMetaData, function(menuData) {
        buildMenu(menuData);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;