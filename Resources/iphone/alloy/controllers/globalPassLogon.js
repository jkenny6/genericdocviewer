function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function setRememberMeButton() {
        var rememberMe = $.rememberMe;
        rememberMe.addEventListener("click", function() {
            var rememberMe = $.rememberMe;
            rememberMe.backgroundImage = rememberMe.backgroundImage == PRESSED_CHECKBOX ? DEFAULT_CHECKBOX : PRESSED_CHECKBOX;
        });
        rememberMe.backgroundImage = PRESSED_CHECKBOX;
    }
    function handleRememberMe(valueofuser) {
        var rememberMe = $.rememberMe;
        rememberMe.backgroundImage == PRESSED_CHECKBOX ? mac_file.writeUserNameToDisk(valueofuser) : mac_file.deleteUserName();
    }
    function createWebView() {
        Ti.Network.createHTTPClient().clearCookies(LOGIN_URL);
        hiddenWebViewSettings = {
            url: LOGIN_URL,
            left: -10,
            top: -10,
            width: 0,
            height: 0,
            hideLoadIndicator: true
        };
        visibleWebViewSettings = {
            url: LOGIN_URL,
            left: 10,
            top: 10,
            width: 600,
            height: 200,
            hideLoadIndicator: true
        };
        webview = Ti.UI.createWebView(visibleWebViewSettings);
        Alloy.Globals.igscWebView = webview;
        webview.addEventListener("error", function(e) {
            hideIndicator();
            if (e.code == NO_INTERNET_CONNECTION_CODE) {
                Alloy.Globals.online = false;
                alert("Internet connection not currently available");
            } else alert(e.message);
        });
        webview.addEventListener("load", function(e) {
            Alloy.Globals.online = true;
            if (Alloy.Globals.loggedOn) {
                var returnedURL = e.url;
                -1 != returnedURL.indexOf(JIVE_LOGIN_URL) && webview.evalJS("document.getElementById('submit-web').click()");
            } else if (globals.isAndroid()) {
                if ("" == urlReturnedFromWebview || urlReturnedFromWebview != webview.url) {
                    urlReturnedFromWebview = webview.url;
                    mac_file.writeWebviewUrlToDisk(urlReturnedFromWebview);
                }
            } else if (globals.isiOs()) {
                var returnedHTML = webview.evalJS("document.getElementsByTagName('body')[0].outerHTML;");
                var returnedURL = e.url;
                returnedURL && (-1 != returnedURL.indexOf(JIVE_LOGIN_URL) ? webview.evalJS("document.getElementById('submit-web').click()") : -1 != returnedURL.indexOf(LOGIN_REDIRECT) || -1 == returnedURL.indexOf(SUCCESS_LOGIN_GENERIC) || handleSuccessLogin(user, password));
                if (returnedHTML) if (-1 != returnedHTML.indexOf("Your User ID or Password is invalid")) {
                    alert("Your User ID or Password is invalid, they are case sensitive.");
                    reset();
                } else if (-1 != returnedHTML.indexOf("404: File Not Found")) {
                    alert("404: File Not Found");
                    reset();
                } else if (-1 != returnedURL.indexOf(FAIL_LOGIN_URL)) {
                    alert("There was an error logging in, please try again.");
                    reset();
                }
            }
        });
        $.globalPassLogon.add(webview);
    }
    function successfullyLoginOffline(user, password) {
        var cachedPassword = "";
        user = user.toUpperCase();
        cachedPassword = Titanium.App.Properties.getString(user, "");
        if ("" !== cachedPassword) {
            if (cachedPassword === password) {
                Alloy.Globals.loggedOn = true;
                webviewWindow.close();
                Ti.App.fireEvent("logonSuccess");
                hideIndicator();
                return true;
            }
            alert("Sorry.  The password you have entered did not match your cached password.  This might be because CAPS LOCK in on,  or the cached password is older than a new Gobal Pass password you are trying to use.  Try your previous  Global Pass password.");
            hideIndicator();
            return false;
        }
        alert("You need to have logged in successfully at least once whilst online in order to facilitate logging in offline using cached credentials");
        hideIndicator();
        reset();
    }
    function reset() {
        webview.setUrl(LOGIN_URL);
        hideIndicator();
    }
    function hideIndicator() {
        $.submitId.enabled = true;
        ui_notifications.hideLoadingIndicator($.globalPassLogon);
    }
    function showIndicator() {
        $.submitId.enabled = false;
        var args = {
            text: "Authenticating..."
        };
        ui_notifications.showLoadingIndicator(args, $.globalPassLogon);
    }
    function notifySuccessfulLogon(user, password) {
        Alloy.Globals.loggedOn = true;
        Titanium.App.Properties.setString(user.toUpperCase(), password);
        webviewWindow.close();
        Ti.App.fireEvent("logonSuccess");
        hideIndicator();
    }
    function handlePostError(returnedHTML) {
        if (null == returnedHTML) alert("There was an error logging in, please check username password and try again."); else if (-1 != returnedHTML.indexOf("Your User ID or Password is invalid")) {
            alert("Your User ID or Password is invalid");
            reset();
        } else if (-1 != returnedHTML.indexOf("404: File Not Found")) {
            alert("404: File Not Found");
            reset();
        } else alert("There was an error logging in, please check username password and try again.");
    }
    function handleSuccessLogin(user, password) {
        notifySuccessfulLogon(user, password);
    }
    function httpPost(user, password) {
        showIndicator();
        var client = Titanium.Network.createHTTPClient();
        client.onload = function() {
            var returnedHTML = this.responseText;
            -1 != returnedHTML.indexOf(SUCCESS_LOGIN_URL_ANDROID) ? handleSuccessLogin(user, password) : handlePostError(returnedHTML);
        };
        client.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        globals.isiOs() && client.setRequestHeader("User-Agent", IOS_USERAGENT);
        client.onerror = function(e) {
            hideIndicator();
            if (null != e.message) alert("Error: " + e.message); else {
                var returnedHTML = this.responseText;
                handlePostError(returnedHTML);
            }
        };
        client.open("POST", urlReturnedFromWebview);
        client.send({
            USER: user,
            PASSWORD: password
        });
    }
    function setSubmitButton() {
        $.submitId.title = "Sign In";
        $.submitId.addEventListener("click", function() {
            user = $.userId.value;
            password = $.passwordId.value;
            if (globals.isEmptyString(user) || globals.isEmptyString(password)) {
                alert("User Name and Password fields cannot be blank");
                hideIndicator();
            } else {
                handleRememberMe(user);
                showIndicator();
                webview.evalJS("document.getElementsByName('USER')[0].value='" + $.userId.value + "';");
                webview.evalJS("document.getElementsByName('PASSWORD')[0].value='" + $.passwordId.value + "';");
                var valueofuser = webview.evalJS("document.getElementsByName('USER')[0].value;");
                if (valueofuser != user) {
                    ui_notifications.displayToast("Logging on using cached credentials.");
                    successfullyLoginOffline(user, password);
                } else globals.isAndroid() ? httpPost(user, password) : globals.isiOs() && webview.evalJS("document.getElementsByName('Login')[0].submit();");
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "globalPassLogon";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.globalPassLogon = Ti.UI.createWindow({
        navTintColor: "#000000",
        navBarHidden: false,
        backgroundImage: "formula1Racing2BW.png",
        width: "100%",
        height: "100%",
        id: "globalPassLogon"
    });
    $.__views.globalPassLogon && $.addTopLevelView($.__views.globalPassLogon);
    $.__views.logo = Ti.UI.createImageView(function() {
        var o = {};
        Alloy.isHandheld && _.extend(o, {
            top: 20,
            left: 20,
            width: Ti.UI.SIZE,
            image: "/csclogo.png",
            canScale: true
        });
        Alloy.isTablet && _.extend(o, {
            top: 50,
            left: 50,
            width: Ti.UI.SIZE,
            image: "/csclogo.png",
            canScale: true
        });
        _.extend(o, {
            id: "logo"
        });
        return o;
    }());
    $.__views.globalPassLogon.add($.__views.logo);
    $.__views.scrollViewContainer = Ti.UI.createScrollView(function() {
        var o = {};
        Alloy.isTablet && _.extend(o, {
            layout: "vertical",
            width: "50%",
            height: Ti.UI.SIZE
        });
        Alloy.isHandheld && _.extend(o, {
            layout: "vertical",
            width: "80%",
            height: Ti.UI.SIZE
        });
        _.extend(o, {
            id: "scrollViewContainer"
        });
        return o;
    }());
    $.__views.globalPassLogon.add($.__views.scrollViewContainer);
    $.__views.logonContainer = Ti.UI.createView({
        backgroundColor: "#cc0d6391",
        layout: "vertical",
        width: "100%",
        height: Ti.UI.SIZE,
        id: "logonContainer"
    });
    $.__views.scrollViewContainer.add($.__views.logonContainer);
    $.__views.logonBody = Ti.UI.createView(function() {
        var o = {};
        Alloy.isTablet && _.extend(o, {
            layout: "vertical",
            height: Ti.UI.SIZE,
            left: 20,
            right: 20,
            bottom: 20
        });
        Alloy.isHandheld && _.extend(o, {
            layout: "vertical",
            height: Ti.UI.SIZE,
            left: 20,
            right: 20,
            bottom: 20
        });
        _.extend(o, {
            id: "logonBody"
        });
        return o;
    }());
    $.__views.logonContainer.add($.__views.logonBody);
    $.__views.userId = Ti.UI.createTextField(function() {
        var o = {};
        Alloy.isTablet && _.extend(o, {
            top: 20,
            paddingLeft: 5,
            height: 50,
            width: Ti.UI.FILL,
            color: "#bbbbbb",
            backgroundColor: "transparent",
            borderWidth: 2,
            borderRadius: 2,
            borderColor: "#bbbbbb",
            hintText: "Username",
            font: {
                fontFamily: "Arial",
                fontSize: "18sp"
            },
            autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE
        });
        Alloy.isHandheld && _.extend(o, {
            top: 20,
            paddingLeft: 5,
            height: 50,
            width: Ti.UI.FILL,
            color: "#bbbbbb",
            backgroundColor: "transparent",
            borderRadius: 1,
            borderColor: "#bbbbbb",
            hintText: "Username h i",
            font: {
                fontFamily: "Arial",
                fontSize: "14sp"
            },
            autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE
        });
        _.extend(o, {
            id: "userId"
        });
        return o;
    }());
    $.__views.logonBody.add($.__views.userId);
    $.__views.passwordId = Ti.UI.createTextField(function() {
        var o = {};
        Alloy.isTablet && _.extend(o, {
            top: 10,
            paddingLeft: 5,
            height: 50,
            width: Ti.UI.FILL,
            color: "#bbbbbb",
            backgroundColor: "transparent",
            borderWidth: 2,
            borderRadius: 2,
            borderColor: "#bbbbbb",
            hintText: "Password",
            passwordMask: true,
            font: {
                fontFamily: "Arial",
                fontSize: "18sp"
            }
        });
        Alloy.isHandheld && _.extend(o, {
            top: 10,
            paddingLeft: 5,
            height: 50,
            width: Ti.UI.FILL,
            color: "#bbbbbb",
            backgroundColor: "transparent",
            borderRadius: 1,
            borderColor: "#bbbbbb",
            hintText: "Password",
            passwordMask: true,
            font: {
                fontFamily: "Arial",
                fontSize: "14sp"
            }
        });
        _.extend(o, {
            id: "passwordId"
        });
        return o;
    }());
    $.__views.logonBody.add($.__views.passwordId);
    $.__views.globalPasstext = Ti.UI.createLabel({
        right: 0,
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        text: "*please use your CSC Global Pass",
        color: "white",
        font: Alloy.CFG.fonts.small,
        id: "globalPasstext"
    });
    $.__views.logonBody.add($.__views.globalPasstext);
    $.__views.rememberMeContainer = Ti.UI.createView(function() {
        var o = {};
        Alloy.isTablet && _.extend(o, {
            layout: "horizontal",
            top: 30,
            left: 0,
            height: Ti.UI.SIZE,
            width: Ti.UI.FILL
        });
        Alloy.isHandheld && _.extend(o, {
            layout: "horizontal",
            top: 20,
            left: 0,
            height: Ti.UI.SIZE,
            width: Ti.UI.FILL
        });
        _.extend(o, {
            id: "rememberMeContainer",
            layout: "horizontal"
        });
        return o;
    }());
    $.__views.logonBody.add($.__views.rememberMeContainer);
    $.__views.rememberMe = Ti.UI.createButton(function() {
        var o = {};
        Alloy.isHandheld && _.extend(o, {
            width: 20,
            height: 20,
            borderRadius: 1,
            borderColor: "#000",
            backgroundColor: "transparent"
        });
        Alloy.isTablet && _.extend(o, {
            width: 40,
            height: 40,
            borderRadius: 1,
            borderColor: "#bbbbbb",
            backgroundColor: "transparent"
        });
        _.extend(o, {
            id: "rememberMe"
        });
        return o;
    }());
    $.__views.rememberMeContainer.add($.__views.rememberMe);
    $.__views.rememberMeLabel = Ti.UI.createLabel(function() {
        var o = {};
        Alloy.isTablet && _.extend(o, {
            color: "#bbbbbb",
            width: Ti.UI.FILL,
            height: 40,
            font: {
                fontFamily: "Arial",
                fontSize: "17sp"
            },
            left: 10
        });
        Alloy.isHandheld && _.extend(o, {
            color: "#bbbbbb",
            width: Ti.UI.FILL,
            height: 25,
            font: {
                fontFamily: "Arial",
                fontSize: "13sp"
            },
            left: 5
        });
        _.extend(o, {
            id: "rememberMeLabel",
            text: "Remember Me"
        });
        return o;
    }());
    $.__views.rememberMeContainer.add($.__views.rememberMeLabel);
    $.__views.submitId = Ti.UI.createButton(function() {
        var o = {};
        Alloy.isTablet && _.extend(o, {
            top: 30,
            bottom: 30,
            paddingLeft: 5,
            paddingRight: 5,
            height: Ti.UI.SIZE,
            width: 80,
            color: "#ffffff",
            borderRadius: 0,
            backgroundColor: "#cc850057",
            font: {
                fontFamily: "Arial",
                fontSize: "19sp",
                fontWeight: "bold"
            }
        });
        Alloy.isHandheld && _.extend(o, {
            top: 20,
            right: 0,
            paddingLeft: 5,
            paddingRight: 5,
            height: Ti.UI.SIZE,
            width: 70,
            color: "#ffffff",
            borderRadius: 0,
            backgroundColor: "cc850057",
            font: {
                fontFamily: "Arial",
                fontSize: "17sp",
                fontWeight: "bold"
            }
        });
        _.extend(o, {
            id: "submitId",
            right: "0"
        });
        return o;
    }());
    $.__views.scrollViewContainer.add($.__views.submitId);
    $.__views.version = Ti.UI.createLabel({
        id: "version",
        bottom: "10",
        right: "10",
        text: "v2.0"
    });
    $.__views.globalPassLogon.add($.__views.version);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var globals = require("globals");
    var ui_notifications = require("ui_notifications");
    var mac_file = require("macfile");
    var webviewWindow = Ti.UI.createWindow();
    var user = "";
    var password = "";
    var NO_INTERNET_CONNECTION_CODE = -1009;
    var webview;
    var DEFAULT_CHECKBOX = "/box.png";
    var PRESSED_CHECKBOX = "/box-checked.png";
    var LOGIN_URL = "http://c3.csc.com";
    var FAIL_LOGIN_URL = "https://gp.amer.csc.com/siteminderagent/forms/login_error3.fcc";
    var JIVE_LOGIN_URL = "https://c3.csc.com/mobile/mobile-redirect.jspa?nativeURL=jivecore";
    var SUCCESS_LOGIN_URL_ANDROID = "https://c3.csc.com/saml/sso";
    var SUCCESS_LOGIN_GENERIC = "https://c3.csc.com/";
    var LOGIN_REDIRECT = "https://c3.csc.com/mobile/mobile-redirect";
    Alloy.Globals.loggedOn = false;
    Alloy.Globals.online = true;
    createWebView();
    setRememberMeButton();
    setSubmitButton();
    globals.isAndroid() && Ti.App.addEventListener(Ti.App.event_androidBack, function() {
        webviewWindow.close();
    });
    var storedUsername = mac_file.getUserNameFromDisk();
    storedUsername && ($.userId.value = storedUsername);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;