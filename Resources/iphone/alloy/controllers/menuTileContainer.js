function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "menuTileContainer";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.menuTileContainer = Ti.UI.createView({
        id: "menuTileContainer"
    });
    $.__views.menuTileContainer && $.addTopLevelView($.__views.menuTileContainer);
    $.__views.comingSoon = Ti.UI.createView({
        touchEnabled: false,
        zIndex: 100,
        backgroundColor: "transparent",
        left: 0,
        bottom: 15,
        width: 168,
        height: 44,
        id: "comingSoon"
    });
    $.__views.menuTileContainer.add($.__views.comingSoon);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    null === args.top || "" === args.top || ($.menuTileContainer.top = args.top);
    null === args.left || "" === args.left || ($.menuTileContainer.left = args.left);
    null === args.bottom || "" === args.bottom || ($.menuTileContainer.bottom = args.bottom);
    null === args.right || "" === args.right || ($.menuTileContainer.right = args.right);
    null === args.height || "" === args.height || ($.menuTileContainer.height = args.height);
    null === args.width || "" === args.width || ($.menuTileContainer.width = args.width);
    $.menuTileContainer.borderColor = null === args.borderColor || "" === args.borderColor ? args.borderColor : "transparent";
    $.menuTileContainer.backgroundColor = null === args.backgroundColor || "" === args.backgroundColor ? args.backgroundColor : "transparent";
    if (args.tileDetails) {
        args.tileDetails.quarter && ($.comingSoon.backgroundImage = args.tileDetails.quarter);
        var menuTile = Alloy.createController("menuTile", args.tileDetails, args.translucent).getView();
        $.menuTileContainer.add(menuTile);
        menuTile.addEventListener("click", function() {});
    }
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;