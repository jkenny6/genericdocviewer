function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "consolePopover";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.consolePopover = Ti.UI.iPad.createPopover({
        title: "Kermit",
        height: "500",
        width: "1000",
        id: "consolePopover"
    });
    $.__views.consolePopover && $.addTopLevelView($.__views.consolePopover);
    $.__views.consolePopover.rightNavButton = void 0;
    $.__views.consoleTextAreaContainer = Ti.UI.createView({
        id: "consoleTextAreaContainer",
        backgroundColor: "black"
    });
    $.__views.consolePopover.add($.__views.consoleTextAreaContainer);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.consolePopover.contentView = $.consoleTextAreaContainer;
    $.consoleTextAreaContainer.add(Alloy.Globals.ConsoleTextArea);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;