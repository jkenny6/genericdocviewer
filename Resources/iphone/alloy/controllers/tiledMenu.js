function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "tiledMenu";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.tiledMenu = Ti.UI.createView({
        id: "tiledMenu"
    });
    $.__views.tiledMenu && $.addTopLevelView($.__views.tiledMenu);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    args.top && ($.tiledMenu.top = args.top);
    args.left && ($.tiledMenu.left = args.left);
    args.bottom && ($.tiledMenu.bottom = args.bottom);
    args.right && ($.tiledMenu.right = args.right);
    $.tiledMenu.backgroundColor = "transparent";
    args.tilesData && _.each(args.tilesData, function(tile) {
        var menuTileContainerController = Alloy.createController("menuTileContainer", tile);
        var menuTileContainerView = menuTileContainerController.getView();
        $.tiledMenu.add(menuTileContainerView);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;