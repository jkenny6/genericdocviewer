function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function hideGlobalPass() {
        var globalPassWindow = $.globalPassLogonId.getView();
        globalPassWindow.close();
    }
    function buildMenu(menuData) {
        var tilesMenuView = Alloy.createController("tiledMenu", menuData.menu).getView();
        $.tiledMenuContainer.add(tilesMenuView);
        tilesMenuView.addEventListener("click", function(e) {
            var selectionsMetaData = {
                tagName: e.source.tagName,
                fileType: e.source.fileType,
                version: e.source.version,
                fileName: e.source.fileName
            };
            var win = null;
            win = Alloy.createController("genericMenu", selectionsMetaData).getView();
            Alloy.Globals.navWin.openWindow(win, {
                animate: true
            });
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    if (true && Alloy.isTablet) {
        $.__views.mainWin = Ti.UI.createWindow({
            backgroundImage: "formula1Racing2BW.png",
            id: "mainWin",
            title: "Americas FY15 Sales Playbook",
            tintColor: "red"
        });
        $.__views.__alloyId3 = Ti.UI.createView({
            backgroundColor: Alloy.CFG.colors.cyber,
            borderColor: "white",
            width: Ti.UI.SIZE,
            height: 30,
            layout: "horizontal",
            id: "__alloyId3"
        });
        $.__views.__alloyId4 = Ti.UI.createLabel({
            width: 300,
            color: "white",
            font: Alloy.CFG.fonts.igscSmall,
            textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
            text: "Americas FY15 Sales Playbook",
            id: "__alloyId4"
        });
        $.__views.__alloyId3.add($.__views.__alloyId4);
        $.__views.mainWin.titleControl = $.__views.__alloyId3;
        $.__views.tiledMenuContainer = Ti.UI.createView({
            id: "tiledMenuContainer"
        });
        $.__views.mainWin.add($.__views.tiledMenuContainer);
        $.__views.navigationControl = Ti.UI.iOS.createNavigationWindow({
            window: $.__views.mainWin,
            id: "navigationControl"
        });
        $.__views.navigationControl && $.addTopLevelView($.__views.navigationControl);
    }
    $.__views.globalPassLogonId = Alloy.createController("globalPassLogon", {
        id: "globalPassLogonId"
    });
    $.__views.globalPassLogonId && $.addTopLevelView($.__views.globalPassLogonId);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var fileManager = require("fileManager");
    Ti.App.addEventListener("logonSuccess", function() {
        hideGlobalPass();
    });
    Alloy.Globals.navWin = $.navigationControl;
    $.navigationControl.open();
    var rootMenuMetaData = {
        tagName: "index",
        fileType: "menudata",
        version: 1,
        fileName: "index.json"
    };
    fileManager.getMenuData(rootMenuMetaData, function(menuData) {
        buildMenu(menuData);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;