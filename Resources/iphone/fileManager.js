function getPdfAndMetaDataFromCloud(menuMetaData, pathsDictionary, displayPdfCallback) {
    Cloud.Files.query({
        page: 1,
        per_page: 20,
        where: {
            tagName: menuMetaData.tagName
        }
    }, function(e) {
        if (e.success) {
            var file = e.files[0];
            if (file) if (file.custom_fields.version > menuMetaData.version) {
                var latestmetaDataFromCloud = {
                    tagName: menuMetaData.tagName,
                    category: file.custom_fields.category,
                    fileType: file.custom_fields.fileType,
                    version: file.custom_fields.version,
                    fileName: menuMetaData.fileName
                };
                getRemotePdf(file.url, pathsDictionary, latestmetaDataFromCloud, displayPdfCallback);
                Ti.API.log("file.custom_fields.category: " + file.custom_fields.category);
                Ti.API.log("latestmetaDataFromCloud.category: " + latestmetaDataFromCloud.category);
                Ti.API.log("file.url: " + file.url);
            } else {
                var localFileName = menuMetaData.fileName;
                Ti.API.log("menuMetaData.category1: >>>" + menuMetaData.category + "<");
                getLocalpdf(localFileName, pathsDictionary, menuMetaData, displayPdfCallback);
            }
        } else {
            var localFileName = menuMetaData.fileName;
            Ti.API.log("menuMetaData.category2: >>>" + menuMetaData.category + "<");
            getLocalpdf(localFileName, pathsDictionary, menuMetaData, displayPdfCallback);
        }
    });
}

function getLocalpdf(localFileName, pathsDictionary, menuMetaData, displayPdfCallback) {
    var pathAndFileName = "";
    var f = Ti.Filesystem.getFile(pdfAppDir, localFileName);
    if (true === f.exists()) {
        pathAndFileName = pdfAppDir + localFileName;
        pathsDictionary.docPath = pathAndFileName;
        displayPdfCallback(menuMetaData, pathsDictionary);
    } else {
        f = Ti.Filesystem.getFile(pdfResDir, localFileName);
        if (true === f.exists()) {
            pathAndFileName = pdfResDir + localFileName;
            pathsDictionary.docPath = pathAndFileName;
            displayPdfCallback(menuMetaData, pathsDictionary);
        } else alert("No File available locally called " + pdfResDir + localFileName);
    }
}

function getRemotePdf(url, pathsDictionary, latestmetaDataFromCloud, displayPdfCallback) {
    function createMenuDataDirs() {
        var dir = Ti.Filesystem.getFile(menuDataResDir);
        dir.exists() || dir.createDirectory();
        dir = Ti.Filesystem.getFile(menuDataAppDir);
        dir.exists() || dir.createDirectory();
        dir = Ti.Filesystem.getFile(pdfResDir);
        dir.exists() || dir.createDirectory();
        dir = Ti.Filesystem.getFile(pdfAppDir);
        dir.exists() || dir.createDirectory();
    }
    function downloadPdfFile() {
        var jsMethod = "[getRemotePdf][downloadPdfFile]";
        console.log(jsFile + jsMethod);
        try {
            if (Titanium.Network.online) {
                var c = Titanium.Network.createHTTPClient();
                c.setTimeout(TIMEOUT);
                c.onload = function() {
                    if (200 == c.status) {
                        var f = Ti.Filesystem.getFile(pdfAppDir, latestmetaDataFromCloud.fileName);
                        f.write(this.responseData);
                        Titanium.App.Properties.setObject(latestmetaDataFromCloud.tagName, latestmetaDataFromCloud);
                        pathsDictionary.docPath = pdfAppDir + latestmetaDataFromCloud.fileName;
                        displayPdfCallback(latestmetaDataFromCloud, pathsDictionary);
                    } else {
                        console.log("[fileManager.js][getRemotePdf][downloadPdfFile]Error:File not found");
                        drawMenuCallback({
                            success: false,
                            id: photoid,
                            error: "[fileManager.js][getRemotePdf][downloadPdfFile]Error:File not found"
                        });
                    }
                };
                c.ondatastream = function() {};
                c.error = function(e) {
                    console.log("[fileManager.js][getRemotePdf][downloadPdfFile]Error:" + e.error);
                    drawMenuCallback({
                        success: false,
                        id: photoid,
                        error: "[fileManager.js][getRemotePdf][downloadPdfFile]Error:" + e.error
                    });
                };
                c.open("GET", url);
                c.send();
            } else {
                console.log("[fileManager.js][getRemotePdf][downloadPdfFile]Error:No Internet");
                drawMenuCallback({
                    success: false,
                    id: photoid,
                    error: "[fileManager.js][getRemotePdf][downloadPdfFile]Error:No Internet"
                });
            }
        } catch (err) {
            console.log("[fileManager.js][getRemotePdf][downloadPdfFile]Error:" + err.message);
            drawMenuCallback({
                success: false,
                id: photoid,
                error: "[fileManager.js][getRemotePdf][downloadPdfFile]Error:" + err.message
            });
        }
    }
    try {
        createMenuDataDirs();
        downloadPdfFile();
    } catch (err) {
        console.log("[fileManager.js][getRemoteMenuJsonData]Error:" + err.message);
    }
}

function getMenuAndMetaDataFromCloud(menuMetaData, drawMenuCallback) {
    Ti.API.log(menuMetaData);
    Cloud.Files.query({
        page: 1,
        per_page: 20,
        where: {
            fileType: menuMetaData.fileType,
            tagName: menuMetaData.tagName
        }
    }, function(e) {
        if (e.success) {
            var file = e.files[0];
            if (file) {
                var remoteVersionNumber = parseInt(file.custom_fields.version);
                var localVersionNumber = parseInt(menuMetaData.version);
                if (remoteVersionNumber > localVersionNumber) {
                    var latestmetaDataFromCloud = {
                        tagName: menuMetaData.tagName,
                        category: file.custom_fields.category,
                        fileType: file.custom_fields.fileType,
                        version: file.custom_fields.version,
                        fileName: menuMetaData.fileName
                    };
                    "menudata" === latestmetaDataFromCloud.fileType ? getRemoteMenuJsonData(file.url, latestmetaDataFromCloud, drawMenuCallback) : alert("Unexpected fileType >" + menuMetaData.fileType + "< encountered for tagName " + menuMetaData.tagName);
                } else {
                    Ti.API.info(menuMetaData.fileName);
                    var localFileName = menuMetaData.fileName;
                    Ti.API.info("-------------------->6" + localFileName);
                    getLocalMenuJsonData(localFileName, drawMenuCallback);
                }
            } else console.log("fileType " + menuMetaData.fileType + " tagName " + menuMetaData.tagName + " was not found");
        } else {
            console.log("Cloud Error:\n" + (e.error && e.message || JSON.stringify(e)));
            var localFileName = menuMetaData.fileName;
            getLocalMenuJsonData(localFileName, drawMenuCallback);
        }
    });
}

function getLocalMenuJsonData(localFileName, drawMenuCallback) {
    var f = Ti.Filesystem.getFile(menuDataAppDir, localFileName);
    if (false === f.exists()) {
        f = Ti.Filesystem.getFile(menuDataResDir, localFileName);
        false === f.exists() && alert("No JSON File available locally called " + menuDataResDir + localFileName);
    }
    var contents = f.read();
    var strJSON = contents.text;
    var menuAsJson = {};
    try {
        menuAsJson = JSON.parse(strJSON);
        drawMenuCallback(menuAsJson);
    } catch (e) {
        console.log(e);
    }
}

function getRemoteMenuJsonData(url, latestmetaDataFromCloud, drawMenuCallback) {
    function createMenuDataDirs() {
        var dir = Ti.Filesystem.getFile(menuDataResDir);
        dir.exists() || dir.createDirectory();
        dir = Ti.Filesystem.getFile(menuDataAppDir);
        dir.exists() || dir.createDirectory();
        dir = Ti.Filesystem.getFile(pdfResDir);
        dir.exists() || dir.createDirectory();
        dir = Ti.Filesystem.getFile(pdfAppDir);
        dir.exists() || dir.createDirectory();
    }
    function downloadMenuFile() {
        try {
            if (Titanium.Network.online) {
                var c = Titanium.Network.createHTTPClient();
                c.setTimeout(TIMEOUT);
                c.onload = function() {
                    if (200 == c.status) {
                        var f = Ti.Filesystem.getFile(menuDataAppDir, latestmetaDataFromCloud.fileName);
                        f.write(this.responseData);
                        Titanium.App.Properties.setObject(latestmetaDataFromCloud.tagName, latestmetaDataFromCloud);
                        var strJSON = this.responseData;
                        var menuAsJson = {};
                        try {
                            menuAsJson = JSON.parse(strJSON);
                        } catch (e) {
                            alert(e);
                        }
                        drawMenuCallback(menuAsJson);
                    } else {
                        console.log("[fileManager.js][getRemoteMenuJsonData][downloadMenuFile]Error:File not found");
                        drawMenuCallback({
                            success: false,
                            id: photoid,
                            error: "[fileManager.js][getRemoteMenuJsonData][downloadMenuFile]Error:File not found"
                        });
                    }
                };
                c.ondatastream = function() {};
                c.error = function(e) {
                    console.log("[fileManager.js][getRemoteMenuJsonData][downloadMenuFile]Error:" + e.error);
                    drawMenuCallback({
                        success: false,
                        id: photoid,
                        error: "[fileManager.js][getRemoteMenuJsonData][downloadMenuFile]Error:" + e.error
                    });
                };
                c.open("GET", url);
                c.send();
            } else {
                console.log("[fileManager.js][getRemoteMenuJsonData]Error:No Internet");
                drawMenuCallback({
                    success: false,
                    id: photoid,
                    error: "[fileManager.js][getRemoteMenuJsonData]Error:No Internet"
                });
            }
        } catch (err) {
            console.log("[fileManager.js][getRemoteMenuJsonData]Error:" + err.message);
            drawMenuCallback({
                success: false,
                id: photoid,
                error: "[fileManager.js][getRemoteMenuJsonData]Error:" + err.message
            });
        }
    }
    try {
        createMenuDataDirs();
        downloadMenuFile();
    } catch (err) {
        console.log("[fileManager.js][getRemoteMenuJsonData]Error:" + err.message);
    }
}

var Cloud = require("ti.cloud");

var jsFile = "[fileManager]";

var TIMEOUT = 15e3;

var menuDataAppDir = Ti.Filesystem.applicationDataDirectory;

var menuDataResDir = Ti.Filesystem.resourcesDirectory + Ti.Filesystem.separator + "jsonFiles";

var pdfAppDir = Ti.Filesystem.applicationDataDirectory;

var pdfResDir = Ti.Filesystem.resourcesDirectory + Ti.Filesystem.separator + "pdf" + Ti.Filesystem.separator;

exports.getPdf = function(menuMetaData, pathsDictionary, displayPdfCallback) {
    var latestMenuMetaData = Titanium.App.Properties.getObject(menuMetaData.tagName, menuMetaData);
    Titanium.App.Properties.setObject(latestMenuMetaData.tagName, latestMenuMetaData);
    latestMenuMetaData = getPdfAndMetaDataFromCloud(latestMenuMetaData, pathsDictionary, displayPdfCallback);
};

exports.getMenuData = function(menuMetaData, drawMenuCallback) {
    var latestMenuMetaData = Titanium.App.Properties.getObject(menuMetaData.tagName, menuMetaData);
    Titanium.App.Properties.setObject(latestMenuMetaData.tagName, latestMenuMetaData);
    latestMenuMetaData = getMenuAndMetaDataFromCloud(latestMenuMetaData, drawMenuCallback);
};