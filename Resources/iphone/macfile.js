function doDeleteUserName() {
    var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, USERNAME_FILENAME);
    file.exists() && file.deleteFile();
    file = null;
}

function deleteWebviewUrl() {
    var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, WEBVIEW_FILENAME);
    file.exists() && file.deleteFile();
    file = null;
}

function getPhoto(photoid, url, callback) {
    function createPhotosDir() {
        var dir = Ti.Filesystem.getFile(photoDir);
        dir.exists() || dir.createDirectory();
    }
    function downloadPhoto() {
        try {
            if (Titanium.Network.online) {
                var c = Titanium.Network.createHTTPClient();
                c.setTimeout(TIMEOUT);
                c.onload = function() {
                    if (200 == c.status) {
                        var f = Ti.Filesystem.getFile(photoDir, photoid);
                        f.write(this.responseData);
                        callback({
                            success: true,
                            id: photoid,
                            photo: photo
                        });
                    } else {
                        console.log("[macfile.js][getPhoto][downloadPhoto]Error:File not found");
                        callback({
                            success: false,
                            id: photoid,
                            error: "[macfile.js][getPhoto][downloadPhoto]Error:File not found"
                        });
                    }
                };
                c.ondatastream = function() {};
                c.error = function(e) {
                    console.log("[macfile.js][getPhoto][downloadPhoto]Error:" + e.error);
                    callback({
                        success: false,
                        id: photoid,
                        error: "[macfile.js][getPhoto][downloadPhoto]Error:" + e.error
                    });
                };
                c.open("GET", url);
                c.send();
            } else {
                console.log("[macfile.js][downloadPhoto]Error:No Internet");
                callback({
                    success: false,
                    id: photoid,
                    error: "[macfile.js][downloadPhoto]Error:No Internet"
                });
            }
        } catch (err) {
            console.log("[macfile.js][getPhoto][downloadPhoto]Error:" + err.message);
            callback({
                success: false,
                id: photoid,
                error: "[macfile.js][getPhoto][downloadPhoto]Error:" + err.message
            });
        }
    }
    var photo = photoDir + Ti.Filesystem.separator + photoid;
    try {
        createPhotosDir();
        var f = Ti.Filesystem.getFile(photoDir, photoid);
        f.exists() ? callback({
            success: true,
            id: photoid,
            photo: photo
        }) : downloadPhoto(callback);
    } catch (err) {
        console.log("[macfile.js][getPhoto]Error:" + err.message);
        callback({
            success: false,
            id: photoid,
            error: "[macfile.js][getPhoto]Error:" + err.message
        });
    }
}

function getPhotos(photos, callback) {
    function nextPhoto(i) {
        var photo = photos[i];
        "undefined" == typeof photo.id ? callback({
            success: false,
            id: photoid,
            error: "[macfile.js][getPhotos]Error:Invalid Photo ID"
        }) : "undefined" == typeof photo.url ? callback({
            success: false,
            id: photoid,
            error: "[macfile.js][getPhotos]Error:Invalid Photo url"
        }) : getPhoto(photo.id, photo.url, function(data) {
            callback(data);
            ++i < len && nextPhoto(i);
        });
    }
    var len = photos.length;
    len > 0 ? nextPhoto(0) : callback({
        success: false,
        id: "undefined",
        error: "[macfile.js][getPhotos]Error:Not photos"
    });
}

var TIMEOUT = 15e3;

var photoDir = Ti.Filesystem.applicationDataDirectory + Ti.Filesystem.separator + "photos";

var USERNAME_FILENAME = "username";

var WEBVIEW_FILENAME = "webview";

exports.deleteUserName = function() {
    doDeleteUserName();
};

exports.writeUserNameToDisk = function(username) {
    doDeleteUserName();
    var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, USERNAME_FILENAME);
    file.write(username);
    file = null;
};

exports.getUserNameFromDisk = function() {
    var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, USERNAME_FILENAME);
    if (file.exists()) {
        var blob = file.read();
        var username = blob.text;
        file = null;
        blob = null;
        return username;
    }
    file = null;
    return "";
};

exports.writeWebviewUrlToDisk = function(webviewUrl) {
    deleteWebviewUrl();
    var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, WEBVIEW_FILENAME);
    file.write(webviewUrl);
    file = null;
};

exports.getWebviewUrlFromDisk = function() {
    var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, WEBVIEW_FILENAME);
    if (file.exists()) {
        var blob = file.read();
        var webviewUrl = blob.text;
        file = null;
        blob = null;
        return webviewUrl;
    }
    file = null;
    return "";
};

exports.photoExists = function(id) {
    var file = Ti.Filesystem.getFile(photoDir, id);
    return file.exists();
};

exports.deleteFile = function(id) {
    var file = Ti.Filesystem.getFile(photoDir, id);
    try {
        file.deleteFile();
    } catch (err) {
        console.log("[macfile.js][deleteFile]Error:" + err.message);
    }
};

exports.getPhotos = function(photos, callback) {
    getPhotos(photos, callback);
};