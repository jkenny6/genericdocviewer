function createCircleView(color, zIndex, rightMargin, diameter) {
    var radius = diameter / 2;
    var params = {
        width: diameter + "dp",
        height: diameter + "dp",
        borderRadius: radius + "dp",
        borderColor: "transparent",
        backgroundColor: color,
        color: color,
        _type: "centerImage",
        zIndex: zIndex
    };
    rightMargin > 0 && (params.right = rightMargin + "dp");
    var circle = Ti.UI.createView(params);
    return circle;
}

function hideView(view) {
    if (null != view) {
        view.visible = false;
        view.height = "0dp";
        view.width = "0dp";
        view.top = "0dp";
        view.bottom = "0dp";
        view.left = "0dp";
        view.right = "0dp";
    }
}

function getImageDimensions(filePath) {
    if (isAndroid()) {
        var testFile = Ti.Filesystem.getFile(filePath).read();
        var imgWidth = testFile.width;
        var imgHeight = testFile.height;
        return {
            width: imgWidth,
            height: imgHeight
        };
    }
    var imgView = Ti.UI.createImageView({
        image: filePath,
        width: "auto",
        height: "auto"
    });
    var imgWidth = imgView.toImage().width;
    var imgHeight = imgView.toImage().height;
    imgView = null;
    return {
        width: imgWidth,
        height: imgHeight
    };
}

function isiOs() {
    return "iphone" === Ti.Platform.osname || "ipad" === Ti.Platform.osname;
}

function isMobileWeb() {
    return "mobileweb" === Ti.Platform.osname;
}

function isiPad() {
    return "ipad" === Ti.Platform.osname;
}

function isiPhone() {
    return "iphone" === Ti.Platform.osname;
}

function isAndroid() {
    return "android" === Ti.Platform.osname;
}

function isTablet() {
    if (isAndroid()) return Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi > 4 ? true : false;
    if (isiOs()) return isiPad();
}

Titanium.include("constants.js");

exports.getAnimationDict = function(close) {
    if (isAndroid()) {
        var animationDict = {
            activityEnterAnimation: Ti.Android.R.anim.slide_in_left,
            activityExitAnimation: Ti.Android.R.anim.slide_out_right
        };
        return animationDict;
    }
    if (close) {
        var animationDict = {
            transition: Titanium.UI.iPhone.AnimationStyle.CURL_DOWN
        };
        return animationDict;
    }
    var animationDict = {
        transition: Titanium.UI.iPhone.AnimationStyle.CURL_UP
    };
    return animationDict;
};

exports.addImageOverlayToCenter = function() {
    var dotDiameter = 6;
    var rightMarin = 4;
    var circleDiameter = 4 * rightMarin + 3 * dotDiameter;
    var circle = createCircleView("#80000000", 10, 0, circleDiameter);
    var dot1 = createCircleView("#fff", 12, rightMarin, dotDiameter);
    var dot2 = createCircleView("#fff", 12, rightMarin, dotDiameter);
    var dot3 = createCircleView("#fff", 12, rightMarin, dotDiameter);
    var dotsContainer = Ti.UI.createView({
        left: rightMarin + "dp",
        zIndex: 11,
        color: "transparent",
        backgroundColor: "transparent",
        layout: "horizontal",
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        _type: "centerImage"
    });
    dotsContainer.add(dot1);
    dotsContainer.add(dot2);
    dotsContainer.add(dot3);
    circle.add(dotsContainer);
    return circle;
};

exports.addTitleOverLayToImage = function(imageView, title) {
    var charsView = Ti.UI.createView({
        bottom: "0dp",
        zIndex: 2,
        width: Ti.UI.FILL,
        height: "25%",
        backgroundColor: "#80000000"
    });
    var charsName = Ti.UI.createLabel({
        bottom: "0dp",
        text: title,
        zIndex: 3,
        color: "#fff",
        width: Ti.UI.SIZE,
        height: "25%",
        backgroundColor: "transparent",
        font: {
            fontSize: "18sp",
            fontWeight: "bold"
        }
    });
    imageView.add(charsName);
    imageView.add(charsView);
};

exports.getTitleFromType = function(listType) {
    var sectionTitle;
    sectionTitle = "industries" == listType ? "Industry" : "platforms" == listType ? "Platform" : "technologies" == listType ? "Technology" : "All";
    return sectionTitle;
};

exports.isEmptyDict = function(ob) {
    for (var i in ob) return false;
    return true;
};

exports.hideView = function(view) {
    hideView(view);
};

exports.initFilterSection = function(menuData, type) {
    var list = _.where(menuData, {
        type: type
    });
    var finalList = [];
    var macdata = require("macdata");
    for (var i = 0; i < list.length; i++) {
        var listItem = list[i];
        var menuItems = macdata.tagProjects(listItem.name);
        menuItems && menuItems.length > 0 && finalList.push(listItem);
    }
    return finalList;
};

exports.getImageDimensions = function(filePath) {
    getImageDimensions(filePath);
};

exports.findChildViewById = function(view, id) {
    for (var x in view.children) if (view.children[x].id == id) return view.children[x];
    return null;
};

exports.removeAllChildren = function(viewObject) {
    var children = viewObject.children.slice(0);
    for (var i = 0; i < children.length; ++i) viewObject.remove(children[i]);
};

exports.imageResize = function(obj) {
    var photoDir = Ti.Filesystem.applicationDataDirectory + Ti.Filesystem.separator + "photos";
    var f = Ti.Filesystem.getFile(photoDir, obj.id);
    var blob = f.read();
    f = null;
    var rect = {
        width: blob.width,
        height: blob.height
    };
    var thumb = {
        width: obj.width
    };
    thumb.height = rect.height / rect.width * thumb.width;
    return blob.imageAsResized(thumb.width, thumb.height);
};

exports.cropImageMaintainRatio = function(filePath, cropWidth, cropHeight, returnBlob) {
    var dimensions = getImageDimensions(filePath);
    var baseWidth = dimensions.width;
    var baseHeight = dimensions.height;
    if (cropWidth >= baseWidth && cropHeight >= baseHeight) {
        var baseImage = Titanium.UI.createImageView({
            image: filePath,
            width: baseWidth + "dp",
            height: baseHeight + "dp",
            left: "0dp",
            top: "0dp",
            _type: "showcase"
        });
        return returnBlob ? baseImage.toImage() : baseImage;
    }
    var ratio = baseWidth / baseHeight;
    var scaledWidth;
    var scaledHeight;
    if (-1 === cropWidth) {
        scaledHeight = cropHeight;
        scaledWidth = scaledHeight * ratio;
        cropWidth = scaledWidth;
    } else if (-1 === cropHeight) {
        scaledWidth = cropWidth;
        scaledHeight = cropWidth / ratio;
        cropHeight = scaledHeight;
    } else if (cropWidth > cropHeight) {
        scaledWidth = cropWidth;
        scaledHeight = scaledWidth / ratio;
    } else {
        scaledHeight = cropHeight;
        scaledWidth = scaledHeight * ratio;
    }
    scaledHeight = parseInt(scaledHeight);
    scaledWidth = parseInt(scaledWidth);
    var croppedImage;
    if (isAndroid()) {
        var image = Ti.Filesystem.getFile(filePath).read();
        var resizedImage = image.imageAsResized(scaledWidth, scaledHeight);
        if (scaledWidth == cropWidth && scaledHeight == cropHeight) croppedImage = resizedImage; else {
            var imageDict = {
                x: 0,
                y: 0,
                width: cropWidth,
                height: cropHeight
            };
            croppedImage = resizedImage.imageAsCropped(imageDict);
        }
    } else {
        var imageView = Titanium.UI.createImageView({
            image: filePath,
            width: scaledWidth + "dp",
            height: scaledHeight + "dp",
            left: "0dp",
            top: "0dp"
        });
        var cropView = Titanium.UI.createView({
            width: cropWidth,
            height: cropHeight,
            left: "0dp",
            top: "0dp"
        });
        cropView.add(imageView);
        croppedImage = cropView.toImage();
    }
    if (returnBlob) return croppedImage;
    var imageView = Titanium.UI.createImageView({
        image: croppedImage,
        width: cropWidth + "dp",
        height: cropHeight + "dp",
        left: "0dp",
        top: "0dp",
        _type: "showcase"
    });
    return imageView;
};

exports.createErrorMessage = function(err) {
    errorMessage = err.error && err.message || JSON.stringify(err);
    return errorMessage;
};

exports.isEmptyString = function(str) {
    if ("" == str || null == str || void 0 == str) return true;
    return false;
};

exports.isAndroid = function() {
    return isAndroid();
};

exports.isiPhone = function() {
    return isiPhone();
};

exports.isiPad = function() {
    return isiPad();
};

exports.isMobileWeb = function() {
    return isMobileWeb();
};

exports.isiOs = function() {
    return isiOs();
};

exports.isTablet = function() {
    return isTablet();
};

exports.hideKeyboard = function() {
    isAndroid() ? Ti.UI.Android.hideSoftKeyboard() : isiOs();
};

exports.hasConnectivity = function() {
    return Titanium.Network.networkType != Titanium.Network.NETWORK_NONE;
};

var createDelimitedStr = function(names, delimiter) {
    var unique = {};
    var distinct = [];
    for (var i in names) {
        var name = names[i];
        "undefined" == typeof unique[name] && distinct.push(name);
        unique[name] = 0;
    }
    var commaStr = distinct.join(delimiter);
    return commaStr;
};

exports.createCommaSeparatedStr = function(names) {
    return createDelimitedStr(names, ", ");
};

exports.createNewlineSeparatedStr = function(names) {
    return createDelimitedStr(names, "\n");
};

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

String.prototype.toTitleCase = function() {
    var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;
    return this.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title) {
        if (index > 0 && index + match.length !== title.length && match.search(smallWords) > -1 && ":" !== title.charAt(index - 2) && ("-" !== title.charAt(index + match.length) || "-" === title.charAt(index - 1)) && title.charAt(index - 1).search(/[^\s-]/) < 0) return match.toLowerCase();
        if (match.substr(1).search(/[A-Z]|\../) > -1) return match;
        return match.charAt(0).toUpperCase() + match.substr(1);
    });
};