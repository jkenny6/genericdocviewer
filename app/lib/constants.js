//Events to listen for
Ti.App.event_navClick = "navigationClick";
Ti.App.event_launchHome = "launchHome";
Ti.App.event_launchSearch = 'launchSearch';
Ti.App.event_filterControlRowSelected = 'filterControl:rowSelected';
Ti.App.event_globalPassLogon = 'GlobalPassLogon';
Ti.App.event_androidBack = 'android:back';
Ti.App.event_projectDetailsInitialized = 'projectDetails:initialized';
Ti.App.event_sectionResized = 'sectionResized';

Ti.App.event_refreshProject = 'refreshProjects';
Ti.App.event_offeringRowClick = 'offeringRowClick';
Ti.App.event_Ratings = 'ratings';
Ti.App.event_SearchClicked = 'searchClicked';
Ti.App.event_displayFeatured = 'displayFeatured';
Ti.App.event_DownloadClicked = 'downloadClick';
Ti.App.event_DownloadButton = 'downloadButton';
Ti.App.event_HideIndicator = 'hideIndicator';
Ti.App.event_InitHeader = 'initHeader';



