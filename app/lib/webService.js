module.exports.callWebService = callWebService;

function callWebService(url, params){
	/* //params example
    var args = {};
	args.parameter1 = 'blabla'
	args.parameter2 = 'blaaaa';
	*/
	var args = params || null;
	
//module.exports = webService;
//webService.prototype.callWebService = function(url, params) {	
	//Ti.API.info('In the web service');
	// create request
    var xhr = Titanium.Network.createHTTPClient();
    //set timeout
    xhr.setTimeout(10000);

    //Here you set the webservice address and method
    xhr.open('GET', url);

    //set encoding
    xhr.setRequestHeader("Accept", "application/json; charset=utf-8");
	
	 // function to deal with errors
    xhr.onerror = function(e) {
    	//Ti.API.info('web service error: '+e.error);
		Ti.API.fireEvent('webServiceResponse',{status:'error',response:e});
    };

    // function to deal with response
    xhr.onload = function() {
    	//Ti.API.info('web service success');
        var obj = this.responseText;
        //Ti.API.info('results: '+obj);
        Ti.API.fireEvent('webServiceResponse',{status:'success',response:obj});
    };
    
    //send request with parameters
    if (args==null)
    {
    	//Ti.API.info('args undefined');
    	xhr.send();
    }
    else
    {
    	//Ti.API.info('args: '+args);
    	xhr.send(JSON.stringify(args));
    }
    
   
    
};
