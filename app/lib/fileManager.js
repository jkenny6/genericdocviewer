var Cloud = require('ti.cloud');
var jsFile = '[fileManager]';

var TIMEOUT = 15000;
var menuDataAppDir = Ti.Filesystem.applicationDataDirectory;
var menuDataResDir = Ti.Filesystem.resourcesDirectory  + Ti.Filesystem.separator + "jsonFiles";
var pdfAppDir = Ti.Filesystem.applicationDataDirectory;
var pdfResDir = Ti.Filesystem.resourcesDirectory  + Ti.Filesystem.separator + "pdf" + Ti.Filesystem.separator;



//////////////
//Menu Objects
//////////////

//  ======================    PDF    ============================

exports.getPdf = function(menuMetaData, pathsDictionary, displayPdfCallback) {
	var jsMethod = '[getPdf]';
	//console.log(jsFile + jsMethod);
	var latestMenuMetaData = Titanium.App.Properties.getObject(menuMetaData.tagName, menuMetaData);
	
	//if the metaData did not exist,  the getObject method  returns the default value which we sent it.  So in case it was not found (i.e First time in);  save it!
	Titanium.App.Properties.setObject(latestMenuMetaData.tagName, latestMenuMetaData);		
	latestMenuMetaData = getPdfAndMetaDataFromCloud(latestMenuMetaData, pathsDictionary, displayPdfCallback);	

	// callback(fileName, pathsDictionary);
};

// Pass in the metadata  as criteria  and try and get the latest version from cloud
function getPdfAndMetaDataFromCloud(menuMetaData, pathsDictionary, displayPdfCallback){
	var jsMethod = '[getPdfAndMetaDataFromCloud]';

	Cloud.Files.query({
	    page: 1,
	    per_page: 20,
	    //where:{"fileType": menuMetaData.fileType, "tagName": menuMetaData.tagName}
	    where:{"tagName": menuMetaData.tagName}
	    
	}, function (e) {
	    if (e.success) {
	    	var file = e.files[0];
	    	if(file){
	    		if(file.custom_fields.version > menuMetaData.version){
	    			// Construct latest meta Data
	    			var latestmetaDataFromCloud = {
	    											"tagName": menuMetaData.tagName, 
	    											"category": file.custom_fields.category, 
	    											"fileType": file.custom_fields.fileType, 
	    											"version":  file.custom_fields.version, 
	    											"fileName": menuMetaData.fileName 
	    										  };
	    										  
	    			// Sure we should be saving latest metaData here now we have it. !
		    		getRemotePdf(file.url, pathsDictionary, latestmetaDataFromCloud,  displayPdfCallback);
		    		Ti.API.log('file.custom_fields.category: ' + file.custom_fields.category);
		    		Ti.API.log('latestmetaDataFromCloud.category: ' + latestmetaDataFromCloud.category);
		    		Ti.API.log('file.url: ' + file.url);
		    		
	    		}
	    		else {
		    		var localFileName = menuMetaData.fileName;
		    		Ti.API.log('menuMetaData.category1: >>>' + menuMetaData.category + '<');
		    		getLocalpdf(localFileName, pathsDictionary, menuMetaData, displayPdfCallback);	    			
	    		}
	    	}
	    } 
	    else {
	        
    		var localFileName = menuMetaData.fileName;
		    Ti.API.log('menuMetaData.category2: >>>' + menuMetaData.category + '<');
		    getLocalpdf(localFileName, pathsDictionary, menuMetaData, displayPdfCallback);	    			
			//getLocalMenuJsonData(localFileName, drawMenuCallback);
	    }
	});	
}

function getLocalpdf(localFileName, pathsDictionary, menuMetaData, displayPdfCallback){
	var jsMethod = '[getLocalpdf]';
	var pathAndFileName = '';
	
	var f = Ti.Filesystem.getFile(pdfAppDir, localFileName);
	if(f.exists()===true) {
		pathAndFileName = pdfAppDir + localFileName;
		pathsDictionary.docPath = pathAndFileName;
		displayPdfCallback(menuMetaData, pathsDictionary);	
	}
	else {
		f = Ti.Filesystem.getFile(pdfResDir, localFileName);
		if(f.exists()===true) {
			pathAndFileName = pdfResDir + localFileName;
			pathsDictionary.docPath = pathAndFileName;
			displayPdfCallback(menuMetaData, pathsDictionary);	
		}
		else {
			alert('No File available locally called ' + pdfResDir + localFileName);
		}
	}
}



function getRemotePdf(url, pathsDictionary, latestmetaDataFromCloud, displayPdfCallback){
	var jsMethod = '[getRemotePdf]';	
	
	try{
		createMenuDataDirs();
		downloadPdfFile();
	}
	catch(err){
		console.log('[fileManager.js][getRemoteMenuJsonData]Error:' + err.message);
	}
	
	
	function createMenuDataDirs() {
		var jsMethod = '[getRemotePdf][createMenuDataDirs]';	
		
		var dir = Ti.Filesystem.getFile(menuDataResDir);
		if (!dir.exists()) {
			dir.createDirectory();
		}

		dir = Ti.Filesystem.getFile(menuDataAppDir);
		if (!dir.exists()) {
			dir.createDirectory();
		}	

		dir = Ti.Filesystem.getFile(pdfResDir);
		if (!dir.exists()) {
			dir.createDirectory();
		}
		dir = Ti.Filesystem.getFile(pdfAppDir);
		if (!dir.exists()) {
			dir.createDirectory();
		}				
	}
	
	function downloadPdfFile(){
		var jsMethod = '[getRemotePdf][downloadPdfFile]';	
		console.log(jsFile + jsMethod);

		try {
			if (Titanium.Network.online) {
				var c = Titanium.Network.createHTTPClient();
				c.setTimeout(TIMEOUT);
				c.onload = function() {
					if (c.status == 200) {
							var f = Ti.Filesystem.getFile(pdfAppDir, latestmetaDataFromCloud.fileName);
							f.write(this.responseData);
							
							
							// Save New MetaData now
							Titanium.App.Properties.setObject(latestmetaDataFromCloud.tagName, latestmetaDataFromCloud);
							
							pathsDictionary.docPath = pdfAppDir + latestmetaDataFromCloud.fileName;
							displayPdfCallback(latestmetaDataFromCloud, pathsDictionary);							

					} else {
						console.log('[fileManager.js][getRemotePdf][downloadPdfFile]Error:File not found');
						drawMenuCallback({
							'success' : false,
							'id' : photoid,
							'error' : '[fileManager.js][getRemotePdf][downloadPdfFile]Error:File not found'
						});
					}

				};
				c.ondatastream = function(e) {
					//TODO implement progress bar
					//e.progress
				};
				c.error = function(e) {
					console.log('[fileManager.js][getRemotePdf][downloadPdfFile]Error:' + e.error);
					drawMenuCallback({
						'success' : false,
						'id' : photoid,
						'error' : '[fileManager.js][getRemotePdf][downloadPdfFile]Error:' + e.error
					});
				};
				c.open('GET', url);
				c.send();
			} else {
				console.log('[fileManager.js][getRemotePdf][downloadPdfFile]Error:No Internet');
				drawMenuCallback({
					'success' : false,
					'id' : photoid,
					'error' : '[fileManager.js][getRemotePdf][downloadPdfFile]Error:No Internet'
				});
			}
		} catch(err) {
			console.log('[fileManager.js][getRemotePdf][downloadPdfFile]Error:' + err.message);
			drawMenuCallback({
				'success' : false,
				'id' : photoid,
				'error' : '[fileManager.js][getRemotePdf][downloadPdfFile]Error:' + err.message
			});
		}
	}	
};


//  ======================    JSON    ============================
exports.getMenuData = function(menuMetaData, drawMenuCallback) {

	var latestMenuMetaData = Titanium.App.Properties.getObject(menuMetaData.tagName, menuMetaData);

	//if the metaData did not exist,  the getObject method  returns the default value which we sent it.  So in case it was not found (i.e First time in);  save it!	
	Titanium.App.Properties.setObject(latestMenuMetaData.tagName, latestMenuMetaData);
	latestMenuMetaData = getMenuAndMetaDataFromCloud(latestMenuMetaData, drawMenuCallback);	
};


// Pass in the cached version  as criteria  and try and get the latest version from cloud
function getMenuAndMetaDataFromCloud(menuMetaData, drawMenuCallback){
	var jsMethod = '[getMenuAndMetaDataFromCloud]';
	
	Ti.API.log(menuMetaData);	



	Cloud.Files.query({
	    page: 1,
	    per_page: 20,
	    where:{"fileType": menuMetaData.fileType, "tagName": menuMetaData.tagName}
	}, function (e) {
	    if (e.success) {
	    	var file = e.files[0];
	    	if(file){

	    		//console.log("fileType " +  menuMetaData.fileType + " tagName " +  menuMetaData.tagName + ' WAS found');
	    		//console.log("file.custom_fields.version " +  file.custom_fields.version + " menuMetaData.version " +  menuMetaData.version);

	    		// Not sure these values are actually ints so lets make sure they are
	    		var remoteVersionNumber = parseInt(file.custom_fields.version);
	    		var localVersionNumber = parseInt(menuMetaData.version);
	    		
	    		if(remoteVersionNumber > localVersionNumber){
	    			//console.log('Getting new version of file');
	    			// Construct latest meta Data
	    			//var latestmetaDataFromCloud = {"tagName": menuMetaData.tagName, "fileType": menuMetaData.fileType, "version": file.custom_fields.version, "fileName": menuMetaData.fileName };

	    			var latestmetaDataFromCloud = {
	    											"tagName": menuMetaData.tagName, 
	    											"category": file.custom_fields.category, 
	    											"fileType": file.custom_fields.fileType, 
	    											"version":  file.custom_fields.version, 
	    											"fileName": menuMetaData.fileName 
	    										  };

	    			if(latestmetaDataFromCloud.fileType === 'menudata'){
		    			// Download file from cloud
		    			getRemoteMenuJsonData(file.url, latestmetaDataFromCloud, drawMenuCallback);
	    				
	    			}
	    			else {
	    				alert('Unexpected fileType >' + menuMetaData.fileType + '< encountered for tagName ' + menuMetaData.tagName);
	    			}
	    		}
	    		else {
	    			//console.log('Sticking with old version of file');
	    			//console.log(menuMetaData);
		    		

		    		Ti.API.info(menuMetaData.fileName);
		    		var localFileName = menuMetaData.fileName;
		    		Ti.API.info('-------------------->6' + localFileName);

		    		getLocalMenuJsonData(localFileName, drawMenuCallback);   			


	    		}
	    	}
	    	else{
	    		console.log("fileType " +  menuMetaData.fileType + " tagName " +  menuMetaData.tagName + ' was not found');
	    	}
	    } 
	    else {
	        console.log('Cloud Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
	        
    		var localFileName = menuMetaData.fileName;
			getLocalMenuJsonData(localFileName, drawMenuCallback);
	    }
	});	
}

function getLocalMenuJsonData(localFileName, drawMenuCallback){
	var jsMethod = '[getLocalMenuJsonData]';
	var pathAndFileName = '';
	
	var f = Ti.Filesystem.getFile(menuDataAppDir,localFileName);
	if(f.exists()===false) {
		// If a version of the file has never been downloaded from cloud,  use the one burnt into the app.
		f = Ti.Filesystem.getFile(menuDataResDir,localFileName);
		if(f.exists()===false) {
			alert('No JSON File available locally called ' + menuDataResDir + localFileName);
		}
	}
	
		
	var contents = f.read();
	var strJSON = contents.text;
	var menuAsJson = {};
	
	try{
		menuAsJson = JSON.parse(strJSON);
		drawMenuCallback(menuAsJson);			
	}
	catch(e){
		console.log(e);
	}	
}


function getRemoteMenuJsonData(url, latestmetaDataFromCloud, drawMenuCallback){
	var jsMethod = '[getRemoteMenuJsonData]';	
	//console.log(jsFile + jsMethod);
	
	try{
		createMenuDataDirs();
		downloadMenuFile();
	}
	catch(err){
		console.log('[fileManager.js][getRemoteMenuJsonData]Error:' + err.message);
	}
	
	
	function createMenuDataDirs() {
		var jsMethod = '[getRemoteMenuJsonData][createMenuDataDirs]';	
		//console.log(jsFile + jsMethod);
		
		var dir = Ti.Filesystem.getFile(menuDataResDir);
		if (!dir.exists()) {
			dir.createDirectory();
		}

		dir = Ti.Filesystem.getFile(menuDataAppDir);
		if (!dir.exists()) {
			dir.createDirectory();
		}	

		dir = Ti.Filesystem.getFile(pdfResDir);
		if (!dir.exists()) {
			dir.createDirectory();
		}
		dir = Ti.Filesystem.getFile(pdfAppDir);
		if (!dir.exists()) {
			dir.createDirectory();
		}					
	}
	
	function downloadMenuFile(){
		var jsMethod = '[getRemoteMenuJsonData][downloadMenuFile]';	
		//console.log(jsFile + jsMethod);

		try {
			if (Titanium.Network.online) {
				var c = Titanium.Network.createHTTPClient();
				c.setTimeout(TIMEOUT);
				c.onload = function() {
					if (c.status == 200) {

							var f = Ti.Filesystem.getFile(menuDataAppDir, latestmetaDataFromCloud.fileName);
							f.write(this.responseData);
							
							// Save New MetaData now
							Titanium.App.Properties.setObject(latestmetaDataFromCloud.tagName, latestmetaDataFromCloud);
	
							var strJSON = this.responseData;
							var menuAsJson = {};
							
							try{
								menuAsJson = JSON.parse(strJSON);	
							}
							catch(e){
								alert(e);
							}
							drawMenuCallback(menuAsJson);							

					} else {
						console.log('[fileManager.js][getRemoteMenuJsonData][downloadMenuFile]Error:File not found');
						drawMenuCallback({
							'success' : false,
							'id' : photoid,
							'error' : '[fileManager.js][getRemoteMenuJsonData][downloadMenuFile]Error:File not found'
						});
					}
				};
				c.ondatastream = function(e) {
					//TODO implement progress bar
					//e.progress
				};
				c.error = function(e) {
					console.log('[fileManager.js][getRemoteMenuJsonData][downloadMenuFile]Error:' + e.error);
					drawMenuCallback({
						'success' : false,
						'id' : photoid,
						'error' : '[fileManager.js][getRemoteMenuJsonData][downloadMenuFile]Error:' + e.error
					});
				};
				c.open('GET', url);
				c.send();
			} else {
				console.log('[fileManager.js][getRemoteMenuJsonData]Error:No Internet');
				drawMenuCallback({
					'success' : false,
					'id' : photoid,
					'error' : '[fileManager.js][getRemoteMenuJsonData]Error:No Internet'
				});
			}
		} catch(err) {
			console.log('[fileManager.js][getRemoteMenuJsonData]Error:' + err.message);
			drawMenuCallback({
				'success' : false,
				'id' : photoid,
				'error' : '[fileManager.js][getRemoteMenuJsonData]Error:' + err.message
			});
		}
	}	
};


