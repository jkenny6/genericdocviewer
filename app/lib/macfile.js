var TIMEOUT = 15000;
var photoDir = Ti.Filesystem.applicationDataDirectory + Ti.Filesystem.separator + 'photos';
var USERNAME_FILENAME = "username";
var WEBVIEW_FILENAME = "webview";

//Username storage
function doDeleteUserName() {
	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, USERNAME_FILENAME);
	if (file.exists()) {
		file.deleteFile();
	}
	file = null;
}

exports.deleteUserName = function() {
	doDeleteUserName();
};

exports.writeUserNameToDisk = function(username) {
	doDeleteUserName();
	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, USERNAME_FILENAME);
	file.write(username);
	file = null;
};

exports.getUserNameFromDisk = function() {
	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, USERNAME_FILENAME);
	if (file.exists()) {
		var blob = file.read();
		var username = blob.text;
		file = null;
		blob = null;
		return username;
	}
	file = null;
	return "";
};

//Webview storage
function deleteWebviewUrl () {
	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, WEBVIEW_FILENAME);
	if (file.exists()) {
		file.deleteFile();
	}
	file = null;
};

exports.writeWebviewUrlToDisk = function(webviewUrl) {
	deleteWebviewUrl();
	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, WEBVIEW_FILENAME);
	file.write(webviewUrl);
	file = null;
};

exports.getWebviewUrlFromDisk = function() {
	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, WEBVIEW_FILENAME);
	if (file.exists()) {
		var blob = file.read();
		var webviewUrl = blob.text;
		file = null;
		blob = null;
		return webviewUrl;
	}
	file = null;
	return "";
};

//Photos
exports.photoExists = function(id) {
	var file = Ti.Filesystem.getFile(photoDir, id);
	return file.exists();
};

exports.deleteFile = function(id) {
	var file = Ti.Filesystem.getFile(photoDir, id);
	try {
		file.deleteFile();
	} catch (err) {
		console.log('[macfile.js][deleteFile]Error:' + err.message);
	}
};


/***
 *
 * photos: array of photos {id:'photo_id, url:'photo_url}
 * callback: callback is called once per photo in the array
 */
exports.getPhotos = function(photos, callback) {
	getPhotos(photos, callback);
};

function getPhoto(photoid, url, callback) {
	var photo = photoDir + Ti.Filesystem.separator + photoid;
	try {
		createPhotosDir();
		var f = Ti.Filesystem.getFile(photoDir, photoid);
		if (f.exists()) {
			callback({
				'success' : true,
				'id' : photoid,
				'photo' : photo
			});
		} else {
			downloadPhoto(callback);
		}
	} catch(err) {
		console.log('[macfile.js][getPhoto]Error:' + err.message);
		callback({
			'success' : false,
			'id' : photoid,
			'error' : '[macfile.js][getPhoto]Error:' + err.message
		});
	}

	function createPhotosDir() {
		var dir = Ti.Filesystem.getFile(photoDir);
		if (!dir.exists()) {
			dir.createDirectory();
		}
	}

	function downloadPhoto() {
		try {
			if (Titanium.Network.online) {
				var c = Titanium.Network.createHTTPClient();
				c.setTimeout(TIMEOUT);
				c.onload = function() {
					if (c.status == 200) {
						var f = Ti.Filesystem.getFile(photoDir, photoid);
						f.write(this.responseData);
						callback({
							'success' : true,
							'id' : photoid,
							'photo' : photo
						});
					} else {
						console.log('[macfile.js][getPhoto][downloadPhoto]Error:File not found');
						callback({
							'success' : false,
							'id' : photoid,
							'error' : '[macfile.js][getPhoto][downloadPhoto]Error:File not found'
						});
					}
				};
				c.ondatastream = function(e) {
					//TODO implement progress bar
					//e.progress
				};
				c.error = function(e) {
					console.log('[macfile.js][getPhoto][downloadPhoto]Error:' + e.error);
					callback({
						'success' : false,
						'id' : photoid,
						'error' : '[macfile.js][getPhoto][downloadPhoto]Error:' + e.error
					});
				};
				c.open('GET', url);
				c.send();
			} else {
				console.log('[macfile.js][downloadPhoto]Error:No Internet');
				callback({
					'success' : false,
					'id' : photoid,
					'error' : '[macfile.js][downloadPhoto]Error:No Internet'
				});
			}
		} catch(err) {
			console.log('[macfile.js][getPhoto][downloadPhoto]Error:' + err.message);
			callback({
				'success' : false,
				'id' : photoid,
				'error' : '[macfile.js][getPhoto][downloadPhoto]Error:' + err.message
			});
		}
	}

};

function getPhotos(photos, callback) {
	var len = photos.length;
	if (len > 0) {
		nextPhoto(0);
	} else {
		callback({
			'success' : false,
			'id' : 'undefined',
			'error' : '[macfile.js][getPhotos]Error:Not photos'
		});
	}

	function nextPhoto(i) {
		var photo = photos[i];
		if ( typeof photo.id === 'undefined') {
			callback({
				'success' : false,
				'id' : photoid,
				'error' : '[macfile.js][getPhotos]Error:Invalid Photo ID'
			});
		} else if ( typeof photo.url === 'undefined') {
			callback({
				'success' : false,
				'id' : photoid,
				'error' : '[macfile.js][getPhotos]Error:Invalid Photo url'
			});
		} else {
			getPhoto(photo.id, photo.url, function(data) {
				callback(data);
				if (++i < len) {
					nextPhoto(i);
				}
			});
		}
	}

}
