exports.definition = {
    config : {
        columns : {
            "id": "INTEGER PRIMARY KEY",
            "conditionText":"TEXT",
            "conditionURI" : "TEXT",
            "conditionParent": "INTEGER"
        },
        adapter : {
            type : "sql",
            collection_name : "healthAZConditions",
            idAttribute: "id"
        }
    }
};