var fileManager = require('fileManager');
var menuMetaData = arguments[0] || {};


fileManager.getMenuData(menuMetaData, 
	function(menuData){
		buildMenu(menuData);
	}
);


function buildMenu(menuData){

	// This one is displayed in the title control
	$.menuTitle.text = menuData.menu.menuTitle;
	$.titleControlContainer.backgroundColor = menuData.menu.titleColor;
	
	// This one is needed for the back button text  of the window that was navigated TO, from this window.
	$.genericMenu.title = menuData.menu.menuTitle;

	var tilesMenuView = Alloy.createController('tiledMenu', menuData.menu).getView();
	$.tiledMenuContainer.add(tilesMenuView);
	
	var pathsDictionary = {};
	
	tilesMenuView.addEventListener('click', function(e){
	
		pathsDictionary.docPath = '';
		pathsDictionary.webPath = '';
		
		var nextLevelDownMetaData = {"tagName": e.source.tagName, "category":e.source.fileType, "fileType": e.source.fileType, "version": e.source.version, "fileName": e.source.fileName };
		
		if(nextLevelDownMetaData.fileType == 'menudata'){
			openNextLevelDownMenu(nextLevelDownMetaData);			
		}
		else {
			
			if(e.source.docPath){
				pathsDictionary.docPath = e.source.docPath;
			}
			
			if(e.source.webPath){
				pathsDictionary.webPath = e.source.webPath;
			}
			
			
			if(pathsDictionary.docPath == ''){
				// No point trying to get a pdf if there is non specified so go straight to 
				// pdf viewer whhch handles this with a message and tells user to use the wiki links instead				
				displayPdfCallBack(nextLevelDownMetaData, pathsDictionary);
			}
			else {
				openPdf(nextLevelDownMetaData, pathsDictionary);
			}
			
		}	
	});

}


function openNextLevelDownMenu(menuMetaData)
{
	var win = null;

	win = Alloy.createController('genericMenu', menuMetaData).getView();
	Alloy.Globals.navWin.openWindow(win,{animate:true});	
}


function openPdf(nextLevelDownMetaData, pathsDictionary){
	fileManager.getPdf(
						nextLevelDownMetaData, 
						pathsDictionary,
						displayPdfCallBack
	);	
}

// Paths Dictionary also has web path as well as 
function displayPdfCallBack(nextLevelDownMetaData, pathsDictionary){
	alert(nextLevelDownMetaData);
			
	//win = Alloy.createController('movieViewer', pathsDictionary).getView();	
	win = Alloy.createController('pdfViewer', pathsDictionary).getView();	
	Alloy.Globals.navWin.openWindow(win,{animate:true});		
	
}






