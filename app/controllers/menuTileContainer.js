var args = arguments[0] || {};





// Set the size of the menu itself
if(!(args.top === null || args.top === '')){
	$.menuTileContainer.top = args.top;	
}


if(!(args.left === null || args.left === '')){
	$.menuTileContainer.left = args.left;	
}


if(!(args.bottom === null || args.bottom === '')){
	$.menuTileContainer.bottom = args.bottom;	
}

if(!(args.right === null || args.right === '')){
	$.menuTileContainer.right = args.right;	
}

if(!(args.height === null || args.height === '')){
	$.menuTileContainer.height = args.height;	
}

if(!(args.width === null || args.width === '')){
	$.menuTileContainer.width = args.width;	
}

if(!(args.borderColor === null || args.borderColor === '')){
	$.menuTileContainer.borderColor = 'transparent';	
}
else {
	$.menuTileContainer.borderColor = args.borderColor;	
}

if(!(args.backgroundColor === null || args.backgroundColor === '')){
	$.menuTileContainer.backgroundColor = 'transparent';	
}
else {
	$.menuTileContainer.backgroundColor = args.backgroundColor;	
}
	
//$.menuTileContainer.borderColor = 'red';



if(args.tileDetails){
	//Do the coming Qn  here as it is part of the tile containernot the tile itself
	if(args.tileDetails.quarter){
		$.comingSoon.backgroundImage = args.tileDetails.quarter;
	}
	
	var menuTile = Alloy.createController('menuTile', args.tileDetails,  args.translucent).getView();
		$.menuTileContainer.add(menuTile);
		menuTile.addEventListener('click', function(e){
	});
	
}

