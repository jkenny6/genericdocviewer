//Libraries
var globals = require('globals');
var ui_notifications = require('ui_notifications');
var mac_file = require('macfile');

////
var webviewWindow = Ti.UI.createWindow();
var user = '';
var password = '';
var TIMEOUT = 30000;
var NO_INTERNET_CONNECTION_CODE = -1009;
var webview;
var DEFAULT_CHECKBOX = "/box.png";
var PRESSED_CHECKBOX = "/box-checked.png";

var LOGIN_URL = 'http://c3.csc.com';
var SUCCESS_LOGIN_URL = "https://c3.csc.com/mobile/mobile-access.jspa";
var SUCCESS_LOGIN_URL_IPAD = "https://c3.csc.com/welcome";
var FAIL_LOGIN_URL = "https://gp.amer.csc.com/siteminderagent/forms/login_error3.fcc";
var JIVE_LOGIN_URL = "https://c3.csc.com/mobile/mobile-redirect.jspa?nativeURL=jivecore";
var SUCCESS_LOGIN_URL_ANDROID = "https://c3.csc.com/saml/sso";
var SUCCESS_LOGIN_GENERIC = "https://c3.csc.com/";
var LOGIN_REDIRECT = "https://c3.csc.com/mobile/mobile-redirect";


Alloy.Globals.loggedOn = false;
Alloy.Globals.online = true;
//ACS admin login to create users
var ACSADMIN = 'csc';
var ACSPW = '1234';

/*
if (globals.isAndroid()) {
	var urlReturnedFromWebview = mac_file.getWebviewUrlFromDisk();
}
*/
function setRememberMeButton() {
	var rememberMe = $.rememberMe;

	rememberMe.addEventListener('click', function(e) {
		var rememberMe = $.rememberMe;

		if (rememberMe.backgroundImage == PRESSED_CHECKBOX) {
			rememberMe.backgroundImage = DEFAULT_CHECKBOX;
		} else {
			rememberMe.backgroundImage = PRESSED_CHECKBOX;
		}
	});
	rememberMe.backgroundImage = PRESSED_CHECKBOX;
}

function handleRememberMe(valueofuser) {
	var rememberMe = $.rememberMe;
	if (rememberMe.backgroundImage == PRESSED_CHECKBOX) {
		mac_file.writeUserNameToDisk(valueofuser);
	} else {
		mac_file.deleteUserName();
	}
}

function createWebView() {

	Ti.Network.createHTTPClient().clearCookies(LOGIN_URL);
	hiddenWebViewSettings = {
		url : LOGIN_URL,
		left:-10,
		top:-10,
		width : 0,
		height : 0,
		hideLoadIndicator : true
	};

	visibleWebViewSettings = {
		url : LOGIN_URL,
		left:10,
		top:10,
		width : 600,
		height : 200,
		hideLoadIndicator : true
	};	
	
	webview = Ti.UI.createWebView(visibleWebViewSettings);
	//webview = Ti.UI.createWebView(hiddenWebViewSettings);
	
	// Keep a handle to this webview so that it can be used for links to c3.  It will be logged on to c3 as it is used for logging on to the app here via Global Pass
	//  This means that the user wont have to logon to c3 again if they click on a link.  (As long as the session has not run out)
	Alloy.Globals.igscWebView = webview;

	webview.addEventListener('error', function(e) {

		hideIndicator();
		if (e.code == NO_INTERNET_CONNECTION_CODE) {
			Alloy.Globals.online = false;
			alert('Internet connection not currently available');
		} else {
			alert(e.message);
		}
	});

	webview.addEventListener('load', function(e) {
		Alloy.Globals.online = true;
		
		// We might be using this web view for a link (elsewhere in the app as this webview is attached to Alloy.Globals.igscWebView) insead of logging on,
		// so only run the load code if we are logging on
		if(!Alloy.Globals.loggedOn){
			if (globals.isAndroid()) {
				if (urlReturnedFromWebview == '' || urlReturnedFromWebview != webview.url) {
					urlReturnedFromWebview = webview.url;
					mac_file.writeWebviewUrlToDisk(urlReturnedFromWebview);
				}
			} else if (globals.isiOs()) {
				var returnedHTML = webview.evalJS("document.getElementsByTagName('body')[0].outerHTML;");
				var returnedURL = e.url;
				
				if (returnedURL) {
					//For ios, this screen pops up first.
					if (returnedURL.indexOf(JIVE_LOGIN_URL) != -1) {
						webview.evalJS("document.getElementById('submit-web').click()");
					} else if ( (!(returnedURL.indexOf(LOGIN_REDIRECT) != -1)) && (returnedURL.indexOf(SUCCESS_LOGIN_GENERIC) != -1)) {
					//} else if (returnedURL.indexOf(SUCCESS_LOGIN_URL) != -1 || returnedURL.indexOf(SUCCESS_LOGIN_URL_IPAD) != -1) {
						handleSuccessLogin(user, password);
					}
				}
	
				if (returnedHTML) {
					if (returnedHTML.indexOf('Your User ID or Password is invalid') != -1) {
						alert('Your User ID or Password is invalid, they are case sensitive.');
						reset();
					} else if (returnedHTML.indexOf('404: File Not Found') != -1) {
						alert('404: File Not Found');
						reset();
					} else if (returnedURL.indexOf(FAIL_LOGIN_URL) != -1) {
						alert("There was an error logging in, please try again.");
						reset();
					}
				}
			}
		}
		else{
			// Have to circumvent the JIVE page that pops up even when we are loged on.  This is for the links in pdfViewer. Remember we are recycling this webview
			var returnedURL = e.url;

			if (returnedURL.indexOf(JIVE_LOGIN_URL) != -1) {
				webview.evalJS("document.getElementById('submit-web').click()");
			}
		}
	});

	$.globalPassLogon.add(webview);

}

function successfullyLoginOffline(user, password){
	var cachedPassword = '';
	user = user.toUpperCase();
	
	cachedPassword = Titanium.App.Properties.getString(user, '');
	if(cachedPassword === ''){
		alert('You need to have logged in successfully at least once whilst online in order to facilitate logging in offline using cached credentials');
		hideIndicator();
		
		// Reset in case they try and connect and have another go
		reset();
		
	}
	else{
		if(cachedPassword === password){
			Alloy.Globals.loggedOn = true;
			webviewWindow.close();
			Ti.App.fireEvent('logonSuccess');
			hideIndicator();
			return true;		
		}
		else {
			alert('Sorry.  The password you have entered did not match your cached password.  This might be because CAPS LOCK in on,  or the cached password is older than a new Gobal Pass password you are trying to use.  Try your previous  Global Pass password.');
			hideIndicator();
			return false;
		}		
	}
}

function reset() {
	webview.setUrl(LOGIN_URL);
	hideIndicator();
}

function hideIndicator() {
	$.submitId.enabled = true;
	ui_notifications.hideLoadingIndicator($.globalPassLogon);
}

function showIndicator() {
	$.submitId.enabled = false;
	var args = {
		text : 'Authenticating...'
	};
	ui_notifications.showLoadingIndicator(args, $.globalPassLogon);
}


function notifySuccessfulLogon(user, password) {
	// This is only called after sucessfully logging on, ONLINE
	// We want to save succesful users password now so that we can allow logons when offline.
	Alloy.Globals.loggedOn = true;
	Titanium.App.Properties.setString(user.toUpperCase(), password);
		
	webviewWindow.close();
	Ti.App.fireEvent('logonSuccess');
	hideIndicator();
}

function handlePostError(returnedHTML) {
	if (returnedHTML == null) {
		alert("There was an error logging in, please check username password and try again.");
	} else if (returnedHTML.indexOf('Your User ID or Password is invalid') != -1) {
		alert('Your User ID or Password is invalid');
		reset();
	} else if (returnedHTML.indexOf('404: File Not Found') != -1) {
		alert('404: File Not Found');
		reset();
	} else {
		alert("There was an error logging in, please check username password and try again.");
	}
}

function handleSuccessLogin(user, password) {
	//loginUser(user, password);
	notifySuccessfulLogon(user, password);
}

function httpPost(user, password) {
	showIndicator();

	var client = Titanium.Network.createHTTPClient();

	client.onload = function() {
		var returnedHTML = this.responseText;
		if (returnedHTML.indexOf(SUCCESS_LOGIN_URL_ANDROID) != -1) {
			handleSuccessLogin(user, password);
		} else {
			handlePostError(returnedHTML);
		}
	};

	client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	if (globals.isiOs()) {
		client.setRequestHeader('User-Agent', IOS_USERAGENT);
	}

	client.onerror = function(e) {
		hideIndicator();
		if (e.message != null) {
			alert("Error: " + e.message);
		} else {
			var returnedHTML = this.responseText;
			handlePostError(returnedHTML);
		}
	};

	client.open('POST', urlReturnedFromWebview);

	client.send({
		"USER" : user,
		"PASSWORD" : password
	});
}

function setSubmitButton() {
	//For some reason I can not set the title directly in the xml
	$.submitId.title = "Sign In";

	$.submitId.addEventListener('click', function(e) {
		user = $.userId.value;
		password = $.passwordId.value;
		
		if (globals.isEmptyString(user) || globals.isEmptyString(password)) {
			alert('User Name and Password fields cannot be blank');
			hideIndicator();
		} else {
			handleRememberMe(user);
			showIndicator();
			// Try to add values to login form.  If there is no connection,  form wont exist so test this by getting value back out
			webview.evalJS("document.getElementsByName('USER')[0].value='" + $.userId.value + "';");
			webview.evalJS("document.getElementsByName('PASSWORD')[0].value='" + $.passwordId.value + "';");
			var valueofuser = webview.evalJS("document.getElementsByName('USER')[0].value;");
			
			if (valueofuser != user) {
				ui_notifications.displayToast('Logging on using cached credentials.');
				successfullyLoginOffline(user, password);
				//reset();
			} else {
				if (globals.isAndroid()) {
					httpPost(user, password);
				} else if (globals.isiOs()) {
					webview.evalJS("document.getElementsByName('Login')[0].submit();");
				}
			}
		}
	});
}

//Main

createWebView();

setRememberMeButton();

setSubmitButton();

if (globals.isAndroid()) {
	Ti.App.addEventListener(Ti.App.event_androidBack, function() {
		webviewWindow.close();
	});
}

var storedUsername = mac_file.getUserNameFromDisk();
if (storedUsername) {
	$.userId.value = storedUsername;
}